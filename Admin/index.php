<?php
session_start();

$connect = new mysqli('localhost', 'root', '', 'bloodshed');

$admin_name;

if (!isset($_SESSION['username'])) {
    header('Location: ../static/');
} else {
    $admin_username = $_SESSION['username'];
}

// $last_records = "SELECT * FROM register WHERE created_at <= now() - INTERVAL 1 DAY";
// if($result = $connect->query($last_records)){
//     $latest_records = mysqli_fetch_array($result);
//     echo '<pre>';
//     print_r('Last 24 Hours Records:- '.$latest_records);
//     die();
// } else {
//     echo $connect->error;
//     die();
// }


$all_user_data = array();
$all_organizer_data = array();
// User Module
$user_details = "SELECT * FROM `register`";

if ($user_result = $connect->query($user_details)) {
    $count_user = 0;
    $count_organizer = 0;
    while ($user_data = mysqli_fetch_array($user_result)) {
        if ($user_data['Role'] == 'user') {
            $count_user++;
            array_push($all_user_data, $user_data);
            // array_push($user_timing, strtok($user_data['created_at'], " ")); //strtok($array || string, "white spaces")->
        } else if ($user_data['Role'] == 'admin') {
            $admin_name = $user_data['fullname'];
        } else {
            $count_organizer += 1;
            array_push($all_organizer_data, $user_data);
        }
    }
} else {
    echo $connect->error;
}

// echo '<pre>';

// print_r($all_user_data);

// if(array_search('Patel', array_column($all_user_data, 'username'))){
//     $single_user_data = array_search('Patel', array_column($all_user_data, 'username'));
// }
// echo '*************************';
// print_r($all_user_data[9]);
// die();

// Organizer Module

// tournament Module
$event_details = "SELECT * FROM `tournament_status`";

$total_event = 0;

if ($event_result = $connect->query($event_details)) {
    $event_data = mysqli_fetch_array($event_result);
    $total_event = mysqli_num_rows($event_result);
} else {
    return $connect->error;
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Admin Dashboard</title>
    <link rel="stylesheet" href="../css/bootstrap.css">
    <script src="../Jquery/jquery-3.4.1.js"></script>
    <script src="../js/bootstrap.js"></script>
    <link rel="stylesheet" href="../Font/fontawesome-free-5.12.0-web/css/all.css">
    <link rel="stylesheet" href="admin_css/admin_sidebar.css">
    <link rel="stylesheet" href="admin_css/styleadmin.css">
</head>

<body>
    <div class="wrapper">
        <!-- Sidebar -->
        <?php include 'sidebar.php'; ?>
        <!-- Page Content -->
        <div id="content">
            <!-- sidebar button -->
            <div class="top_navbar position-fixed" style="z-index: 1111;">
                <nav class="navbar navbar-expand-lg navbar-light">
                    <ul class="nav navbar_button">
                        <li class="nav-item">
                            <a href="#!" type="button" id="sidebarCollapse" class="btn bg-dark text-white">
                                <i class="fas fa-align-left"></i>
                                <span></span>
                            </a>
                        </li>
                    </ul>
                    <ul class="nav ml-auto profile">
                        <li class="nav-item profile_icon">
                            <span class="profile_icon_img">
                                <a href="#!">
                                    <img src="../images/profile.jpg" alt="">
                                </a>
                            </span>
                        </li>
                        <li class="nav-item">
                            <a href="#" class="nav-link">
                                <p class=""><?php echo $admin_name; ?></p>
                                <p>Online</p>
                            </a>
                        </li>
                    </ul>
                </nav>
            </div>


            <!-- dashboard heading -->
            <div class="page_heading">
                <h2>Dashboard</h2>
            </div>

            <!-- Admin Info -->
            <div class="admin_welcome text-white">
                <div class="h1 welcome">
                    <span class="h1">Welcome Back</span>
                    <span class="admin_name pl-5"><?php echo $admin_name; ?></span>
                </div>
            </div>

            <!-- 4 block of main dashboard -->
            <div class="container-fluid progress_bar">
                <div class="row rounded">
                    <div class="col-12 pt-1 pb-1">
                        <p>Portfolio Performance</p>
                    </div>

                    <!-- Ist Block -->
                    <div class="col-sm-6 col-md-6 col-lg-3">
                        <div class="user_block d-flex rounded">
                            <!-- Logo -->
                            <div class="user_logo bg-warning">
                                <i class="fas fa-user"></i>
                            </div>
                            <!-- Stats -->
                            <div class="user_count text-right p-3">
                                <p>User</p>
                                <span><?php echo $count_user; ?></span>
                            </div>
                        </div>
                    </div>

                    <!-- 2nd block -->
                    <div class="col-sm-6 col-md-6 col-lg-3">
                        <div class="user_block d-flex rounded">
                            <!-- Logo -->
                            <div class="user_logo bg-warning">
                                <i class="fas fa-user"></i>
                            </div>
                            <!-- Stats -->
                            <div class="user_count text-right p-3">
                                <p>Organizer</p>
                                <span><?php echo $count_organizer; ?></span>
                            </div>
                        </div>
                    </div>

                    <!-- 3rd block -->
                    <div class="col-sm-6 col-md-6 col-lg-3">
                        <div class="user_block d-flex rounded">
                            <!-- Logo -->
                            <div class="user_logo bg-warning">
                                <i class="fas fa-user"></i>
                            </div>
                            <!-- Stats -->
                            <div class="user_count text-right p-3">
                                <p>Events</p>
                                <span><?php echo $total_event; ?></span>
                            </div>
                        </div>
                    </div>

                    <!-- 4th block -->
                    <div class="col-sm-6 col-md-6 col-lg-3">
                        <div class="user_block d-flex rounded">
                            <!-- Logo -->
                            <div class="user_logo">
                                <i class="fas fa-user"></i>
                            </div>
                            <!-- Stats -->
                            <div class="user_count text-right p-3">
                                <p>Daily Activity</p>
                                <span>79</span>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <!-- website summary -->
            <div class="container-fluid website_summary mt-5">
                <div class="table_heading">
                    <i class="fas fa-table"></i>
                </div>
                <table class="table text-white text-center ">
                    <caption style="caption-side: top; text-align: center;">Website summary</caption>
                    <thead>
                        <th>
                            Role
                        </th>
                        <th>
                            Total enrolments
                        </th>
                        <th>
                            Active
                        </th>
                        <th>
                            Unactive
                        </th>
                        <th>
                            Action
                        </th>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Administrator</td>
                            <td>1</td>
                            <td>1</td>
                            <td>0</td>
                            <td>
                                <a href="#!" class="btn bg-danger ">View Full Records</a>
                            </td>
                        </tr>

                        <tr>
                            <td>Organizer</td>
                            <td><?php echo $count_organizer; ?></td>
                            <td><?php
                                $count_non_active_organizer = 0;
                                $count_active_organizer = 0;
                                foreach ($all_organizer_data as $org_data) {
                                    $datetime1 = strtok($org_data['created_at'], " ");
                                    $date = new DateTime($datetime1);
                                    $now = new DateTime();

                                    $day_diff = $date->diff($now)->format("%d");
                                    if ($day_diff > 6) {
                                        $count_non_active_organizer++;
                                    } else {
                                        $count_active_organizer += 1;
                                    }
                                }

                                echo $count_active_organizer;
                                ?></td>
                            <td><?php echo $count_non_active_organizer; ?></td>
                            <td>
                                <a href="organizer/org_data.php" class="btn bg-danger ">View Full Records</a>
                            </td>
                        </tr>

                        <tr>
                            <td>User</td>
                            <td><?php echo $count_user; ?></td>
                            <td><?php
                                $count_non_active_user = 0;
                                $count_active_user = 0;
                                foreach ($all_user_data as $user_data) {
                                    $datetime1 = strtok($user_data['created_at'], " ");
                                    $date = new DateTime($datetime1);
                                    $now = new DateTime();

                                    $day_diff = $date->diff($now)->format("%d");
                                    if ($day_diff > 6) {
                                        $count_non_active_user++;
                                    } else {
                                        $count_active_user += 1;
                                    }
                                }

                                echo $count_active_user;
                                ?></td>
                            <td><?php echo $count_non_active_user; ?></td>
                            <td>
                                <a href="user/user_data.php" target="_blank " class="btn bg-danger ">View Full
                                    Records</a>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>

            <!-- all stats through chart -->

        </div>
    </div>



    <!-- Javascript -->
    <script src="admin_js/sidebar.js "></script>
    <script>
    </script>
</body>

</html>