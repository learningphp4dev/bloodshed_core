<?php
session_start();
date_default_timezone_set('Asia/Kolkata');

$connect = new mysqli('localhost', 'root', '', 'bloodshed');
if (!isset($_SESSION['username'])) {
    header('Location: ../static/');
} else {
    $admin_username = $_SESSION['username'];
}
// $date = str_replace('/', '-', $var);
// echo date('d-m-Y', strtotime($date));
// die();

// fetch all organizer data
$all_org_data = array();
$fetch_all_organizer_event = "SELECT * FROM `tournament_status`";
$result = $connect->query($fetch_all_organizer_event);
while ($row = mysqli_fetch_assoc($result)) {
    array_push($all_org_data, $row);
}


// Approve statement
if (isset($_POST['grant'])) {
    $approve = $_POST['grant'];
    $event_id = $_POST['event_id'];
    $grant = "UPDATE `tournament_status` SET status = '$approve' WHERE event_id = '$event_id'";
    if ($connect->query($grant)) {
        echo "granted";
    } else {
        echo "denied";
    }
    exit();
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Event Priviligies</title>
    <link rel="stylesheet" href="../css/bootstrap.css">
    <script src="../Jquery/jquery-3.4.1.js"></script>
    <script src="../js/bootstrap.js"></script>
    <link rel="stylesheet" href="../Font/fontawesome-free-5.12.0-web/css/all.css">
    <link rel="stylesheet" href="admin_css/admin_sidebar.css">
    <link rel="stylesheet" href="admin_css/event_granted.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

</head>

<body>
    <div class="wrapper">
        <!-- Sidebar -->
        <?php include 'sidebar.php'; ?>
        <!-- Page Content -->
        <div id="content">
            <!-- sidebar button -->
            <div class="top_navbar position-fixed" style="z-index: 1111;">
                <nav class="navbar navbar-expand-lg navbar-light">
                    <ul class="nav navbar_button">
                        <li class="nav-item">
                            <a href="#!" type="button" id="sidebarCollapse" class="btn bg-dark text-white">
                                <i class="fas fa-align-left"></i>
                                <span></span>
                            </a>
                        </li>
                    </ul>
                    <ul class="nav ml-auto profile">
                        <li class="nav-item profile_icon">
                            <span class="profile_icon_img">
                                <a href="#!">
                                    <img src="../images/profile.jpg" alt="">
                                </a>
                            </span>
                        </li>
                        <li class="nav-item">
                            <a href="#" class="nav-link">
                                <p class=""><?php echo $admin_username; ?></p>
                                <p>Online</p>
                            </a>
                        </li>
                    </ul>
                </nav>
            </div>


            <!-- dashboard heading -->
            <div class="page_heading">
                <h2>Event</h2>
            </div>

            <!-- Approved and Ongoing Events -->
            <h1 class="text-center text-white">Ongoing and Upcoming Events</h1>
            <!-- Tabs -->
            <div class="tabs events mt-5">
                <ul class="diamond-cut">
                    <li>
                        <a href="#tab1">
                            <span>Today</span></a>
                    </li>
                    <li>
                        <a href="#tab2">
                            <span>Within Month</span></a>
                    </li>
                </ul>
                <div class="contents">
                    <div id="tab1">
                        <table class="table text-white text-center">
                            <thead>
                                <th>Index</th>
                                <th>Event Name</th>
                                <th>Organizer Name</th>
                                <th>Game Type</th>
                                <th>Date and Time</th>
                                <th>Action</th>
                            </thead>
                            <tbody>
                                <?php
                                $i = 1;
                                foreach ($all_org_data as $key) {
                                    if ($key['status'] == 'granted') {
                                        $datetime1 = strtok($key['start_date'], " ");
                                        $date = new DateTime($datetime1);
                                        $now = new DateTime();

                                        $day_diff = $date->diff($now)->format("%d");
                                        if ($day_diff == '0' && $day_diff == '1') {
                                ?>
                                            <tr>
                                                <td><?php echo $i; ?></td>
                                                <td><?php echo $key['tournament_name']; ?></td>
                                                <td><?php echo $key['username_id']; ?></td>
                                                <td><?php echo $key['type']; ?></td>
                                                <td><?php echo $key['start_date'] . ' ' . $key['time']; ?></td>
                                                <td><a href="<?php echo 'organizer/checkevent.php?org_id=' . $key['event_id']; ?>" class="btn btn-warning">View</a>
                                                    <a href="<?php echo 'delete.php?id=' . $key['event_id']; ?>" class="btn btn-danger">Delete</a></td>
                                            </tr>
                                        <?php
                                            $i++;
                                        } else {
                                        ?>
                                            <tr>
                                                <td colspan="6">No Tournament Available Today !</td>
                                            </tr>
                                <?php
                                            break;
                                        }
                                    }
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>

                    <div id="tab2">
                        <table class="table text-white text-center">
                            <thead>
                                <th>Index</th>
                                <th>Event Name</th>
                                <th>Organizer Name</th>
                                <th>Game Type</th>
                                <th>Date and Time</th>
                                <th>Action</th>
                            </thead>
                            <tbody>
                                <?php
                                $i = 1;
                                foreach ($all_org_data as $key) {
                                    if ($key['status'] == 'granted') {
                                        $datetime1 = strtok($key['start_date'], " ");
                                        $date = new DateTime($datetime1);
                                        $now = new DateTime();

                                        $day_diff = $date->diff($now)->format("%d");
                                        if ($day_diff >= '1') {
                                ?>
                                            <tr>
                                                <td><?php echo $i; ?></td>
                                                <td><?php echo $key['tournament_name']; ?></td>
                                                <td><?php echo $key['username_id']; ?></td>
                                                <td><?php echo $key['type']; ?></td>
                                                <td><?php echo $key['start_date'] . ' ' . $key['time']; ?></td>
                                                <td><a href="<?php echo 'organizer/Organizer_EventInfo.php?event_id=' . $key['event_id']; ?>" class="btn btn-warning">View</a>
                                                    <a href="<?php echo 'delete.php?id=' . $key['event_id']; ?>" class="btn btn-danger">Delete</a></td>

                                            </tr>
                                        <?php
                                            $i++;
                                        } else {
                                        ?>
                                            <tr>
                                                <td colspan="6">No Tournament Available Today !</td>
                                            </tr>
                                <?php
                                            break;
                                        }
                                    }
                                }
                                ?>
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>



            <h1 class="text-center text-white mt-5">Pending Events Request</h1>
            <!-- Pending Rquest Events -->
            <div class="tabs events mt-5">
                <ul class="diamond-cut">
                    <li>
                        <a href="#tab1">
                            <span>Weekly</span></a>
                    </li>

                </ul>
                <div class="contents">
                    <div id="tab1">
                        <table class="table text-white text-center">
                            <thead>
                                <th>Index</th>
                                <th>Event Name</th>
                                <th>Organizer Name</th>
                                <th>Game Type</th>
                                <th>Date and Time</th>
                                <th>Action</th>
                            </thead>
                            <tbody>
                                <?php
                                $i = 1;
                                foreach ($all_org_data as $key) {
                                    if ($key['status'] == 'pending') {
                                ?>
                                        <tr>
                                            <td><?php echo $i; ?></td>
                                            <td><?php echo $key['tournament_name']; ?></td>
                                            <td><?php echo $key['username_id']; ?></td>
                                            <td><?php echo $key['type']; ?></td>
                                            <td><?php echo $key['start_date'] . ' ' . $key['time']; ?></td>
                                            <td>
                                                <button type="button" id="approved" class="btn btn-danger" value="<?php echo $key['event_id']; ?>">Click To Approve</button>
                                                <a href="<?php echo 'organizer/Organizer_EventInfo.php?event_id=' . $key['event_id']; ?>" class="btn btn-warning">View</a>
                                                <a href="<?php echo 'delete.php?id=' . $key['event_id']; ?>" class="btn btn-danger">Delete</a>
                                            </td>
                                        </tr>
                                    <?php
                                        $i++;
                                    } else {
                                    ?>
                                        <tr>
                                            <td colspan="6">No Pending Tournament !</td>
                                        </tr>
                                <?php
                                        break;
                                    }
                                }
                                ?>
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>



    <!-- Javascript -->
    <script src="admin_js/sidebar.js "></script>
    <script type="text/javascript">
        $(function() {
            $(".tabs").tabs({
                show: {
                    effect: "fade",
                    direction: "right"
                },
                hide: {}
            });

            $('#approved').click(function() {
                let event_id = $('#approved').val();
                $.ajax({
                    url: 'event_granted.php',
                    type: 'post',
                    data: {
                        grant: 'granted',
                        event_id: event_id
                    },
                    success: function(data) {
                        if (data == 'granted') {
                            window.location.reload();
                        } else {
                            alert('Something Wrong ! Contact Administrator');
                        }
                    }
                });
            });
        });
    </script>
</body>

</html>