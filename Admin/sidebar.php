 <!-- Sidebar -->
 <nav id="sidebar">

     <div class="sidebar-header">
         <h3 class="mt-5">BLOODSHED</h3>
     </div>

     <!-- sidebar profile picture -->
     <div class="sidebar_profile d-block d-lg-none">
         <div class="img_container">
             <img src="../images/profile.jpg" alt="profile">
         </div>

         <h4 class="admin_welcome">
             <?php echo $admin_name; ?>
         </h4>
     </div>

     <ul class="list-unstyled components">
         <li>
             <a href="http://localhost/bloodshed_core/Admin/index.php">Dashboard</a>
         </li>
         <li>
             <a href="http://localhost/bloodshed_core/Admin/event_granted.php">Event Privilige</a>
         </li>
         <li class="active">
             <a href="#homeSubmenu" data-toggle="collapse" class="dropdown-toggle">Website Pages</a>
             <ul class="collapse list-unstyled" id="homeSubmenu">
                 <li>
                     <a href="#">Home Page</a>
                 </li>
             </ul>
         </li>
         <li>
             <a href="#pageSubmenu" data-toggle="collapse" class="dropdown-toggle">User Module</a>
             <ul class="collapse list-unstyled" id="pageSubmenu">
                 <li>
                     <a href="http://localhost/bloodshed_core/Admin/user/user_data.php">User Data</a>
                 </li>
             </ul>
         </li>
         <li>
             <a href="#org" data-toggle="collapse" class="dropdown-toggle">Organizer Module</a>
             <ul class="collapse list-unstyled" id="org">
                 <li>
                     <a href="http://localhost/bloodshed_core/Admin/organizer/org_data.php">Organizer Data</a>
                 </li>
                 <li>
                     <a href="http://localhost/bloodshed_core/Admin/event_granted.php">Organizer events</a>
                 </li>
             </ul>
         </li>
         <li>
             <a href="http://localhost/bloodshed_core/logout.php">Logout</a>
         </li>
     </ul>

 </nav>