<?php
error_reporting(0);
session_start();
$connect = new mysqli('localhost', 'root', '', 'bloodshed');
if (!isset($_SESSION['username'])) {
    header('Location: ../../static/');
} else {
    $admin_username = $_SESSION['username'];
}
if (isset($_REQUEST['id'])) {
    $id = $_REQUEST['id'];

    $select = "SELECT * FROM `register` WHERE `id` = '$id'";

    $result = $connect->query($select);
    $data = mysqli_fetch_assoc($result);
}

$user_username = $data['username'];

// user event list
$event_list = explode(',',$data['event_list']);
$last_event = end($event_list);

$tournament_status = "SELECT * FROM `tournament_status` WHERE event_id = '$last_event'";
$result = $connect->query($tournament_status);
$event_data = mysqli_fetch_assoc($result);

// tournament records
$all_event_record_data = array();
$fetch_all_event_record = "SELECT * FROM `tournament_record` WHERE username = '$user_username' AND status = 'completed'";
$result = $connect->query($fetch_all_event_record);

while ($row = mysqli_fetch_array($result)) {
    array_push($all_event_record_data, $row);
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="../../css/bootstrap.css">
    <script src="../../Jquery/jquery-3.4.1.js"></script>
    <script src="../../js/bootstrap.js"></script>
    <link rel="stylesheet" href="../../Font/fontawesome-free-5.12.0-web/css/all.css">
    <link rel="stylesheet" href="../admin_css/admin_sidebar.css">
    <link rel="stylesheet" href="u_profile.css">
    <!-- <link rel="stylesheet" href="../../Jquery/jquery-ui.css">
    <script src="../../Jquery/jquey-ui.js"></script> -->
</head>

<body>

    <div class="wrapper">
       <!-- Sidebar -->
       <?php include 'sidebar.php'; ?>
        <!-- Page Content -->
        <div id="content">
            <!-- sidebar button -->
            <div class="top_navbar position-fixed" style="z-index: 1111;">
                <nav class="navbar navbar-expand-lg navbar-light">
                    <ul class="nav navbar_button">
                        <li class="nav-item">
                            <a href="#!" type="button" id="sidebarCollapse" class="btn bg-dark text-white">
                                <i class="fas fa-align-left"></i>
                                <span></span>
                            </a>
                        </li>
                    </ul>
                    <ul class="nav ml-auto profile">
                        <li class="nav-item profile_icon">
                            <span class="profile_icon_img">
                                <a href="#!">
                                    <img src="../../images/profile.jpg" alt="">
                                </a>
                            </span>
                        </li>
                        <li class="nav-item">
                            <a href="#" class="nav-link">
                                <p class=""><?php echo $admin_username; ?></p>
                                <p>Online</p>
                            </a>
                        </li>
                    </ul>
                </nav>
            </div>



            <!-- dashboard heading -->
            <div class="page_heading text-white">
                <h2>User Module</h2>
            </div>

            <!-- Body Content -->
            <div class="container-fluid user_table_content">
                <div class="row">
                    <div class="col-12 p-0 pt-3 pb-3">
                        <h4 class="text-white text-center text-uppercase" style="letter-spacing: 3px;">User Profile Picture
                        </h4>

                        <div class="user_profile">
                            <img src="../../images/pubg.jpg" alt="">
                        </div>
                    </div>
                    <!-- divide table in block -->
                    <div class="col-sm-6 mt-5 last_login">
                        <div class="table_heading">
                            <i class="fas fa-gamepad"></i>
                        </div>
                        <table class="table text-white text-center">
                            <caption style="caption-side: top; border-top: 1px solid #223a41; border-bottom: 1px solid #223a41; font-size: 22px; font-family: sans-serif;" class="text-white pl-3 text-center">
                                Last Event
                            </caption>
                            <thead>
                                <tr>
                                    <th>Last Participate</th>
                                    <th>Start Date</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td><?php echo $event_data['tournament_name']; ?></td>
                                    <td><?php echo strtok($event_data['start_date'], " "); ?></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-sm-6 mt-5 last_game">
                        <div class="table_heading">
                            <i class="fas fa-user"></i>
                        </div>
                        <table class="table text-white text-center">
                            <caption style="caption-side: top; border-top: 1px solid #223a41; border-bottom: 1px solid #223a41; font-size: 22px; font-family: sans-serif;" class="text-white pl-3 text-center">
                                Last Login
                            </caption>
                            <thead>
                                <tr>
                                    <th>Login Date</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>
                                        <?php
                                        echo strtok($data['updated_at'], " ");
                                        ?>
                                    </td>
                                    <?php
                                    $datetime1 = strtok($data['updated_at'], " ");
                                    $date = new DateTime($datetime1);
                                    $now = new DateTime();

                                    $day_diff = $date->diff($now)->format("%d");
                                    if ($day_diff <= 2) {
                                    ?>
                                        <td><button class="btn btn-success">Active</button></td>
                                    <?php
                                    } else {
                                    ?>
                                        <td><button class="btn btn-danger">inactive</button></td>
                                    <?php } ?>
                                </tr>
                            </tbody>
                        </table>
                    </div>

                    <!-- personal information -->
                    <div class="col-12 p-0"></div>
                    <div class="User_data">
                        <div class="table_heading">
                            <i class="fas fa-info-circle"></i>
                        </div>
                        <table class="table text-white text-center">
                            <caption style="caption-side: top; border-top: 1px solid #223a41; border-bottom: 1px solid #223a41; font-size: 22px; font-family: sans-serif;" class="text-white pl-3 text-center">Personal Information</caption>
                            <thead>
                                <tr>
                                    <th>Index</th>
                                    <th>Username</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Mobile</th>
                                    <th>Gender</th>
                                    <th>Participation</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>1</td>
                                    <td><?php echo $data['username']; ?></td>
                                    <td><?php echo $data['fullname']; ?></td>
                                    <td><?php echo $data['email']; ?></td>
                                    <td><?php echo $data['mobile']; ?></td>
                                    <td><?php echo $data['gender']; ?></td>
                                    <td><?php echo count($event_list); ?></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>

                    <!-- event information -->
                    <div class="User_data">
                        <div class="table_heading">
                            <i class="fas fa-table"></i>
                        </div>
                        <table class="table text-white text-center">
                            <caption style="caption-side: top; border-top: 1px solid #223a41; border-bottom: 1px solid #223a41; font-size: 22px; font-family: sans-serif;" class="text-white pl-3 text-center">Event Information</caption>
                            <thead>
                                <tr>
                                    <th>Index</th>
                                    <th>Participate</th>
                                    <th>Win</th>
                                    <th>Runner up</th>
                                    <th>Win %</th>
                                    <th>Winning Prize [Cash + Other in cash]</th>
                                    <th>All Events</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>1</td>
                                    <td><?php echo count($event_list); ?></td>
                                    <td>4</td>
                                    <td>2</td>
                                    <td>30</td>
                                    <td>$5264.45 [$234.45 + $5030]</td>
                                    <td><a href="#!" class="btn btn-warning">View</a></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
    </div>
    <!-- Javascript -->
    <script src="../admin_js/sidebar.js"></script>

</body>

</html>