<?php
session_start();
$connect = new mysqli('localhost', 'root', '', 'bloodshed');
if (!isset($_SESSION['username'])) {
    header('Location: ../../static/');
} else {
    $admin_username = $_SESSION['username'];
}
$organizer_all_data = array();

$organizer_data = "Select * FROM `register`";
$result = $connect->query($organizer_data);
while ($data = mysqli_fetch_assoc($result)) {
    if ($data['Role'] == 'organizer') {
        array_push($organizer_all_data, $data);
    }
}


?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="../../css/bootstrap.css">
    <script src="../../Jquery/jquery-3.4.1.js"></script>
    <script src="../../js/bootstrap.js"></script>
    <link rel="stylesheet" href="../../Font/fontawesome-free-5.12.0-web/css/all.css">
    <link rel="stylesheet" href="org_data.css">
    <link rel="stylesheet" href="../admin_css/admin_sidebar.css">
    </style>
</head>

<body>
    <div class="wrapper">
        <!-- Sidebar -->
        <?php include 'sidebar.php'; ?>
        <!-- Page Content -->
        <div id="content">
            <!-- sidebar button -->
            <div class="top_navbar position-fixed" style="z-index: 1111;">
                <nav class="navbar navbar-expand-lg navbar-light">
                    <ul class="nav navbar_button">
                        <li class="nav-item">
                            <a href="#!" type="button" id="sidebarCollapse" class="btn bg-dark text-white">
                                <i class="fas fa-align-left"></i>
                                <span></span>
                            </a>
                        </li>
                    </ul>
                    <ul class="nav ml-auto profile">
                        <li class="nav-item profile_icon">
                            <span class="profile_icon_img">
                                <a href="#!">
                                    <img src="../../images/profile.jpg" alt="">
                                </a>
                            </span>
                        </li>
                        <li class="nav-item">
                            <a href="#" class="nav-link">
                                <p class="">Gagandeep Singh</p>
                                <p>Online</p>
                            </a>
                        </li>
                    </ul>
                </nav>
            </div>



            <!-- dashboard heading -->
            <div class="page_heading text-white">
                <h2>Organizer Module</h2>
            </div>

            <!-- Input Fields -->
            <div class="container mx-auto input_field">
                <div class="row p-0 m-0">
                    <div class="col-10 col-sm-12 col-md-4 mx-auto">
                        <div class="search-box position-relative">
                            <input type="text" class="box form-control mx-auto" placeholder="search here">
                            <i class="fas fa-search"></i>
                        </div>
                    </div>

                    <div class="select col-10 mx-auto col-md-4">
                        <select class="form-control mx-auto">
                            <option value="">Select status</option>
                            <option value="Active">Active</option>
                            <option value="inactive">inactive</option>
                        </select>
                    </div>

                </div>
            </div>
            <!-- Body Content -->

            <!-- Newly Register -->
            <div class="container-fluid user_table_content">
                <div class="row">
                    <div class="col-12 p-0">
                        <div class="User_data">
                            <div class="table_upper_heading">
                                <h1 class="text-white text-center">Newly Register</h1>
                            </div>
                            <div class="table_heading">
                                <i class="fas fa-table"></i>
                            </div>
                            <table class="table text-white">
                                <caption class="text-white pl-3">Organizer Info</caption>
                                <thead>
                                    <tr>
                                        <th>Index</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Role</th>
                                        <th>Status</th>
                                        <th>View profile</th>
                                        <th>Delete</th>

                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $i = 1;
                                    foreach ($organizer_all_data as $org_data) {
                                        $datetime1 = strtok($org_data['created_at'], " ");
                                        $date = new DateTime($datetime1);
                                        $now = new DateTime();

                                        $day_diff = $date->diff($now)->format("%d");
                                        if ($day_diff <= 2) {
                                    ?>
                                            <tr>
                                                <td><?php echo $i; ?></td>
                                                <td><?php echo $org_data['fullname'] ?></td>
                                                <td><?php echo $org_data['email']; ?></td>
                                                <td><?php echo ucfirst($org_data['Role']); ?></td>
                                                <td>
                                                    <p class="btn btn-success">Active</p>
                                                </td>
                                                <td><button class="btn btn-warning"><a href="<?php echo 'org_profile.php?id=' . $org_data['id']; ?>">View profile</a></button></td>
                                                <td><button class="btn btn-danger"><a href="<?php echo '../delete.php?id=' . $org_data['id']; ?>">Delete</a></button></td>
                                            </tr>
                                    <?php $i++;
                                        } else {
                                        }
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Already Register -->
            <div class="container-fluid user_table_content">
                <div class="row">
                    <div class="col-12 p-0">
                        <div class="User_data">
                            <div class="table_upper_heading">
                                <h1 class="text-white text-center">Already Register</h1>
                            </div>
                            <div class="table_heading">
                                <i class="fas fa-table"></i>
                            </div>
                            <table class="table text-white">
                                <caption class="text-white pl-3">Organizer Info</caption>
                                <thead>
                                    <tr>
                                        <th>Index</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Role</th>
                                        <th>Status</th>
                                        <th>View profile</th>
                                        <th>Delete</th>

                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $i = 1;
                                    foreach ($organizer_all_data as $org_data) {
                                        $datetime1 = strtok($org_data['created_at'], " ");
                                        $date = new DateTime($datetime1);
                                        $now = new DateTime();

                                        $day_diff = $date->diff($now)->format("%d");
                                        if ($day_diff <= 2) {
                                        } else {
                                    ?>
                                            <tr>
                                                <td><?php echo $i; ?></td>
                                                <td><?php echo $org_data['fullname'] ?></td>
                                                <td><?php echo $org_data['email']; ?></td>
                                                <td><?php echo ucfirst($org_data['Role']); ?></td>
                                                <?php
                                                if ($day_diff <= 7) {
                                                ?>
                                                    <td>
                                                        <p class="btn btn-success">Active</p>
                                                    </td>
                                                <?php
                                                } else {
                                                ?>
                                                    <td>
                                                        <p class="btn btn-danger">In-Active</p>
                                                    </td>
                                                <?php
                                                }
                                                ?>
                                                <td><button class="btn btn-warning"><a href="<?php echo 'org_profile.php?id=' . $org_data['id']; ?>">View profile</a></button></td>
                                                <td><button class="btn btn-danger"><a href="<?php echo '../delete.php?id=' . $org_data['id']; ?>">Delete</a></button></td>
                                            </tr>
                                    <?php
                                            $i++;
                                        }
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>


    <!-- Javascript -->
    <script src="../admin_js/sidebar.js"></script>
</body>

</html>