<?php
// error_reporting(0);
session_start();

$connect = new mysqli('localhost', 'root', '', 'bloodshed');

// check user type and session
if (isset($_SESSION['username'])) {
    $role = trim($_SESSION['role']);
    if ($role == 'organizer') {
        $username = $_SESSION['username'];
    } else {
        header('Location: ../static/index.php');
    }
} else {
    header('Location: ../static/index.php');
}

if (isset($_SESSION['username'])) {
    $username = $_SESSION['username'];

    $fetch_user_detail = "SELECT * FROM `register` WHERE `username`= '$username' AND `Role` = 'organizer'";

    $result = $connect->query($fetch_user_detail);
    $user_detail = mysqli_fetch_assoc($result);

    // print_r($user_detail);
    // die();
}

if (isset($_POST['submit'])) {
    echo "<script>alert('php enter');</script>";

    $fullname = $_POST['fullname'];
    $email = $_POST['email'];
    $mobile = $_POST['number'];
    $address = $_POST['address'];
    $aboutme = $_POST['about'];
    $size = $_FILES['image']['size'];
    if ($size > 10240000) {
        echo "sorry file should be less then 100kb";
    } else {
        $name = $_FILES['image']['name'];
        $tmpname = $_FILES['image']['tmp_name'];
        // echo $tmpname;
        $folder = 'images/' . $name;
        move_uploaded_file('../user_profile/' . $tmpname, 'images/' . $name);
    }

    $insert = "UPDATE `register` SET fullname = '$fullname',image = '$folder',mobile = '$mobile', address = '$address', about_me = '$aboutme' WHERE `username` = '$username'";


    if ($connect->query($insert)) {
        // echo 'Success';
    } else {
        echo $connect->error;
    }
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Organizer Dashboard</title>
    <link rel="stylesheet" href="../css/bootstrap.css">
    <script src="../Jquery/jquery-3.4.1.js"></script>
    <script src="../js/bootstrap.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.1/css/all.min.css">
    <script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
    <link rel="stylesheet" href="Org_navbar.css">
    <link rel="stylesheet" href="sidebar.css">
    <link rel="stylesheet" href="org_profile.css">
    <link rel="stylesheet" href="../Font/fontawesome-free-5.12.0-web/css/all.css">
</head>

<body>
    <!-- Main Navbar -->
    <!-- Navbar -->
    <div class="main_navbar">
        <nav class="navbar navbar-expand-lg navbar-mainbg">
            <a class="navbar-brand navbar-logo" href="#">Bloodshed</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <i class="fas fa-bars text-white"></i>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto">
                    <div class="hori-selector">
                        <div class="left"></div>
                        <div class="right"></div>
                    </div>
                    <li class="nav-item active">
                        <a class="nav-link" href="Organizer_Dashboard.php">
                            <i class="fas fa-tachometer-alt"></i> Dashboard
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="org_profile.php">
                            <i class="far fa-address-book"></i> My Profile
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="org_profile.php">
                            <i class="far fa-chart-bar"></i> Edit Profile
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="../logout.php">
                            <i class="far fa-copy"></i> Logout
                        </a>
                    </li>
                </ul>
                <div class="nav-item profile_navbar d-flex ml-auto" style="width: 20%;">
                    <div class="profile">
                        <img src="../images/profile.jpg" style="height: 100%; width: 100%;" alt="">
                    </div>
                    <div class="profile_info text-white pl-2">
                        <span class="username d-block"><?php echo strtoupper($username); ?></span>
                        <span class="user_status d-block position-relative">ONLINE</span>
                    </div>
                </div>
            </div>
        </nav>
    </div>
    <!-- Navbar -->
    <!-- Body Content Start -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 px-0">
                <header>
                    <div class="dashb pt-4 pl-5 text-white text-uppercase text-accent-1 text-center">
                        <h1>Dashboard</h1>
                    </div>
                    <div class="hamburger">
                        <i class="nav_icon fas fa-bars"></i>
                    </div>
                    <?php include 'sidebar.php'; ?>
                </header>
                <main>
                    <div class="zoom-content">
                        <section>
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col-md-7 col-lg-7 col-sm-6 col-12 mt-5 mb-5 mx-auto">
                                        <div class="main-card">
                                            <div class="profile-card" id="profile-card">
                                                <i class="fas fa-edit float-right edit" onclick=""></i><br>
                                                <div class="profile-pic">
                                                    <img src="../images/fn2.jpg" alt="">
                                                </div>
                                                <div class="profile-content">
                                                    <h3 class="title">Disha Sharma</h3>
                                                    <span class="description">Description Lorem ipsum, dolor sit amet consectetur adipisicing elit. em voluptatibus enim.</span>
                                                    <hr>
                                                    <div class="contact-info">
                                                        <div class="mailId"><i class="fas fa-envelope float-left"></i>dishasharma@gmail.com</div>
                                                        <div class="address"><i class="fas fa-home float-left"></i>Lives in San Francisco, CA</div>
                                                        <div class="mobile"><i class="fas fa-phone-alt float-left"></i>+91 654 784 547</div>
                                                    </div>
                                                </div>

                                                <ul class="social">
                                                    <li><a href="" class="fab fa-instagram-square"></a></li>
                                                    <li><a href="" class="fab fa-twitter-square"></a></li>
                                                    <li><a href="" class="fab fa-facebook-square"></a></li>
                                                </ul>

                                            </div>
                                            <div class="edit-card mt-5 mb-5" id="edit-card">
                                                <i class="fas fa-angle-left float-right view" onclick="viewProfile()"></i><br>
                                                <h3>Edit Profile</h3>
                                                <form action="" method="POST" class="input-group" enctype="multipart/form-data">
                                                    <label for="file" class="input-field input-label">
                                                        <span id="label_span" class="float-left">Upload Profile Image</span></label>
                                                    <input type="file" name="image" id="file" required>

                                                    <input class="input-field" type="text" name="fullname" value="<?php if (!empty($user_detail['fullname'])) {
                                                                                                                        echo $user_detail['fullname'];
                                                                                                                    } ?>" placeholder="Your Name" required>
                                                    <textarea name="about" id="" class="input-field" placeholder="About">
                                                    <?php if (!empty($user_detail['about_me'])) {
                                                        echo $user_detail['about_me'];
                                                    } ?>
                                                    </textarea>
                                                    <input class="input-field" type="email" name="email" value="<?php if (!empty($user_detail['email'])) {
                                                                                                                    echo $user_detail['email'];
                                                                                                                } ?>" placeholder="Email Id" readonly>
                                                    <input class="input-field" type="text" name="address" value="<?php if (!empty($user_detail['address'])) {
                                                                                                                        echo $user_detail['address'];
                                                                                                                    } ?>" placeholder="Address">
                                                    <input class="input-field" type="number" name="number" value="<?php if (!empty($user_detail['mobile'])) {
                                                                                                                        echo $user_detail['mobile'];
                                                                                                                    } ?>" placeholder="Contact Number">
                                                    <div class="div-btn mt-2">
                                                        <button type="submit" name="submit" class="submit-btn rounded">Sumbit</button>
                                                    </div>
                                                </form>
                                            </div>

                                        </div>
                                    </div>

                                </div>
                            </div>
                        </section>
                    </div>
                </main>
            </div>
        </div>
    </div>


    <script>
        $(document).ready(function() {
            $('#edit-card').hide();
            $('.edit').click(function() {
                $('#edit-card').show();
                $('#profile-card').hide();
            });
            $('.view').click(function() {
                $('#profile-card').show();
                $('#edit-card').hide();
            });

            // upload image
            $('#file').on('change', function(e) {
                var filename = e.target.value.split('\\').pop();
                $("#label_span").text(filename);

            });
            $('textarea').each(function() {
                $(this).val($(this).val().trim());
            });

        });
    </script>
    <!-- Hamburger Menu Link -->
    <script src="Organizer_Dashboard.js"></script>
    <!-- upper navbar link -->
    <script src="../JavaScript/Organizer_menu.js"></script>
</body>

</html>