<?php
session_start();

$connect = new mysqli('localhost', 'root', '', 'bloodshed');
// check user type and session
if (isset($_SESSION['username'])) {
    $role = trim($_SESSION['role']);
    if ($role == 'organizer') {
        $username = $_SESSION['username'];
    } else {
        header('Location: ../static/index.php');
    }
} else {
    header('Location: ../static/index.php');
}

$pending = "SELECT * FROM `tournament_status` WHERE `username_id`='$username' AND `status` = 'pending'";

$result = $connect->query($pending);


$available = "SELECT * FROM `tournament_status` WHERE `username_id`='$username' AND `status` = 'granted'";

$granted = $connect->query($available);

// $user_data = "SELECT * FROM `register` WHERE `username` = '$username'";
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Create New Event</title>
    <link rel="stylesheet" href="../css/bootstrap.css">
    <script src="../Jquery/jquery-3.4.1.js"></script>
    <script src="../js/bootstrap.js"></script>
    <link rel="stylesheet" href="../Font/fontawesome-free-5.12.0-web/css/all.css">
    <link rel="stylesheet" href="checkeventcss.css">
    <link rel="stylesheet" href="Org_navbar.css">
    <link rel="stylesheet" href="sidebar.css">
    <style>
        .no_event {
            background: #192231;
            box-shadow: -5px -5px 11px rgba(113, 113, 113, 0.15),
                5px 5px 11px rgba(0, 0, 0, .15);
            border-radius: 20px;
            padding: 4rem 1px;
            position: relative;
        }

        .div-centered {
            width: 100%;
            position: absolute;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
        }
    </style>
</head>

<body>
    <!-- Main Navbar -->
    <!-- Main Navbar -->
    <!-- Navbar -->
    <div class="main_navbar">
        <nav class="navbar navbar-expand-lg navbar-mainbg">
            <a class="navbar-brand navbar-logo" href="#">Bloodshed</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <i class="fas fa-bars text-white"></i>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav ml-auto">
                    <div class="hori-selector">
                        <div class="left"></div>
                        <div class="right"></div>
                    </div>
                    <li class="nav-item active">
                        <a class="nav-link" href="Organizer_Dashboard.php">
                            <i class="fas fa-tachometer-alt"></i> Dashboard
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="org_profile.php">
                            <i class="far fa-address-book"></i> My Profile
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="org_profile.php">
                            <i class="far fa-chart-bar"></i> Edit Profile
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="../logout.php">
                            <i class="far fa-copy"></i> Logout
                        </a>
                    </li>
                </ul>

                <div class="nav-item profile_navbar d-flex ml-auto" style="width: 20%;">
                    <div class="profile">
                        <img src="../images/profile.jpg" style="height: 100%; width: 100%;" alt="">
                    </div>
                    <div class="profile_info text-white pl-2">
                        <span class="username d-block"><?php echo strtoupper($username); ?></span>
                        <span class="user_status d-block position-relative">ONLINE</span>
                    </div>
                </div>
            </div>
        </nav>
    </div>

    <!-- Sidebar / Code -->
    <header>

        <div class="hamburger">
            <i class="nav_icon fas fa-bars"></i>
        </div>
        <?php include 'sidebar.php'; ?>
    </header>

    <!-- Body Section / Tabs -->
    <main>
        <div class="zoom-content">
            <div class="container-fluid mt-4">
                <div class="row">
                    <div class="event_first_heading col-12">
                        <h1>Pending Event</h1>
                    </div>
                    <div class="col-6 col-sm-5 col-md-3">
                        <select name="" id="" class="form-control">
                            <option value="">Month</option>
                            <option value="">This Month</option>
                            <option value="">Next Month</option>
                        </select>
                    </div>
                </div>
            </div>

            <!-- cards -->
            <div class="container-fluid mt-5">
                <div class="row">
                    <?php
                    $result = $connect->query($pending);
                    if (mysqli_num_rows($result) <= 0) {
                    ?>
                        <div class="no_event rounded mx-auto" style="height: 60vh; width: 80%;">
                            <div class="div-centered">
                                <h1 class="mb-5">You Haven't Created Any Tournament Yet</h1>
                                <div class="text-center"><a href="Organizer_CreateNewEvent.php" class="btn btn-warning">Create Tournament</a></div>
                            </div>
                        </div>
                        <?php
                    } else {
                        while ($row = mysqli_fetch_assoc($result)) {
                        ?>
                            <div class=" col-sm-6 col-md-4 col-lg-3">
                                <a href="Organizer_EventInfo.php?event_id=<?php echo $row["event_id"]; ?>">
                                    <div class="card">
                                        <img src="<?php echo '../' . $row['banner'] ?>" class="card-img-top" alt="...">
                                        <div class="card-body">
                                            <h5 class="card-title">
                                                <?php
                                                echo strtoupper($row['tournament_name']);
                                                ?>
                                            </h5>
                                            <p class="card-subtitle">
                                                <p>Status: Pending</p>
                                                <p>Prize: $<?php echo $row['price']; ?> /10 Credits</p>
                                                <p>Starts: Wed <?php echo $row['start_date']; ?> @ <?php echo $row['time'] ?> PM IST</p>
                                            </p>
                                            <a href="Organizer_EventInfo.php?event_id=<?php echo $row["event_id"]; ?>" class="btn btn-primary">View</a>
                                        </div>
                                    </div>
                                </a>
                            </div>
                    <?php
                        }
                    }
                    ?>
                </div>
            </div>


            <!-- All event  -->
            <div class="container-fluid mt-4">
                <div class="row">
                    <div class="event_first_heading-2 col-12">
                        <h1>Event</h1>
                    </div>
                    <div class="col-3">
                        <select name="" id="" class="form-control">
                            <option value="">Month</option>
                            <option value="">This Month</option>
                            <option value="">Next Month</option>
                        </select>
                    </div>
                </div>
            </div>


            <!-- cards -->
            <div class="container-fluid mt-5">
                <div class="row">
                    <?php
                    $result = $connect->query($available);
                    if (mysqli_num_rows($result) <= 0) {
                    ?>
                        <div class="no_event rounded mx-auto" style="height: 60vh; width: 80%;">
                            <div class="div-centered">
                                <h1 class="mb-5">Your tournament is pending for permission.</h1>
                            </div>
                        </div>
                        <?php
                    } else {
                        while ($row = mysqli_fetch_assoc($result)) {
                        ?>
                            <div class=" col-sm-6 col-md-4 col-lg-3">
                                <a href="Organizer_EventInfo.php?event_id=<?php echo $row["event_id"]; ?>">
                                    <div class="card">
                                        <img src="<?php echo '../' . $row['banner'] ?>" class="card-img-top" alt="...">
                                        <div class="card-body">
                                            <h5 class="card-title">
                                                <?php
                                                echo strtoupper($row['tournament_name']);
                                                ?>
                                            </h5>
                                            <p class="card-subtitle">
                                                <p>Status: Pending</p>
                                                <p>Prize: $<?php echo $row['price']; ?> /10 Credits</p>
                                                <p>Starts: Wed <?php echo $row['start_date']; ?> @ <?php echo $row['time'] ?> PM IST</p>
                                            </p>
                                            <a href="Organizer_EventInfo.php?event_id=<?php echo $row["event_id"]; ?>" class="btn btn-primary">View</a>
                                            <?php
                                            $datetime1 = strtok($row['start_date'], " ");
                                            $date = new DateTime($datetime1);
                                            $now = new DateTime();

                                            $day_diff = $date->diff($now)->format("%d");
                                            if ($day_diff == '0' || $day_diff == '1') {
                                            ?>
                                                <a href="result_submit.php?event_id=<?php echo $row["event_id"]; ?>" class="btn btn-primary">Submit Result</a>
                                            <?php
                                            }

                                            ?>
                                        </div>
                                    </div>
                                </a>
                            </div>
                    <?php
                        }
                    }
                    ?>
                </div>
            </div>
        </div>
    </main>


    <!-- End of footer -->
    <script src="../JavaScript/Organizer_menu.js"></script>
    <script src="Organizer_Dashboard.js"></script>
</body>

</html>