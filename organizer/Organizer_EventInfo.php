<?php
session_start();
// check user type and session
if (isset($_SESSION['username'])) {
    $role = trim($_SESSION['role']);
    if ($role == 'organizer') {
        $username = $_SESSION['username'];
    } else {
        header('Location: ../static/index.php');
    }
} else {
    header('Location: ../static/index.php');
}
$connect = new mysqli('localhost', 'root', '', 'bloodshed');
$data = array();
if (isset($_REQUEST['event_id'])) {
    $event_id = $_REQUEST['event_id'];

    // echo $event_id . "<br>";

    $select = "SELECT * FROM `tournament_status` WHERE `event_id`='$event_id'";
    if ($result = $connect->query(($select), MYSQLI_USE_RESULT)) {
        $data = mysqli_fetch_assoc($result);
    } else {
        echo $connect->error;
    }
}


// prize and time list json
// json time list file
$file = file_get_contents('../organizer/time_list.json');

$time_array = json_decode($file, true);

// json prize list file
$prize_file = file_get_contents('../organizer/prize_list.json');

$prize_array = json_decode($prize_file, true);

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="../css/bootstrap.css">
    <script src="../Jquery/jquery-3.4.1.js"></script>
    <script src="../js/bootstrap.js"></script>
    <link rel="stylesheet" href="Organizer_EventInfo.css">
    <link rel="stylesheet" href="sidebar.css">
    <link rel="stylesheet" href="Org_navbar.css">
    <link rel="stylesheet" href="../Font/fontawesome-free-5.12.0-web/css/all.css">
    <link rel="stylesheet" href="../static_css/foot.css">
</head>

<body class="bg-dark">

    <!-- Main Navbar -->
    <!-- Navbar -->
    <div class="main_navbar">
        <nav class="navbar navbar-expand-lg navbar-mainbg">
            <a class="navbar-brand navbar-logo" href="#">Bloodshed</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <i class="fas fa-bars text-white"></i>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav ml-auto">
                    <div class="hori-selector">
                        <div class="left"></div>
                        <div class="right"></div>
                    </div>
                    <li class="nav-item active">
                        <a class="nav-link" href="Organizer_Dashboard.php">
                            <i class="fas fa-tachometer-alt"></i> Dashboard
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="org_profile.php">
                            <i class="far fa-address-book"></i> My Profile
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="org_profile.php">
                            <i class="far fa-chart-bar"></i> Edit Profile
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="../logout.php">
                            <i class="far fa-copy"></i> Logout
                        </a>
                    </li>
                </ul>

                <div class="nav-item profile_navbar d-flex ml-auto" style="min-width: 20%;">
                    <div class="profile">
                        <img src="../images/profile.jpg" style="height: 100%; width: 100%;" alt="">
                    </div>
                    <div class="profile_info text-white pl-2">
                        <span class="username d-block"><?php echo strtoupper($username); ?></span>
                        <span class="user_status d-block position-relative">ONLINE</span>
                    </div>
                </div>
            </div>
        </nav>
    </div>
    <!-- Navbar -->
    <!-- Body Content Start -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 px-0">
                <header>
                    <div class="hamburger">
                        <i class="nav_icon fas fa-bars"></i>
                    </div>
                    <?php include 'sidebar.php'; ?>
                </header>
                <main>
                    <div class="zoom-content">

                        <section>
                            <!-- 1st container -->
                            <div class="container-fluid p-0 m-0 ">
                                <!-- Page heading -->
                                <div class="text-white mb-4 text-uppercase text-center dashboard_heading">
                                    <h1>Dashboard</h1>
                                </div>
                                <div class="main_heading">
                                    <h1 class="heading text-white text-center p-2">
                                        EVENT DETAIL
                                    </h1>
                                </div>
                            </div>

                            <!-- 2nd container -->
                            <!-- Logo and Banner -->
                            <div class="container-fluid  pt-3 pb-3 p-0 m-0 logo_banner">
                                <div class="row p-0 m-0">
                                    <!-- Logo -->
                                    <div class="logo col-md-6 position-relative">
                                        <h2>GAME LOGO</h2>
                                        <div class="logo_img">
                                            <img src="<?php echo '../' . $data['logo']; ?>" class="rounded img-fluid" alt="Logo">
                                        </div>
                                    </div>
                                    <!-- Banner -->
                                    <div class="banner col-md-6 position-relative">
                                        <h2>GAME BANNER</h2>
                                        <div class="banner_img">
                                            <img src="<?php echo '../' . $data['banner']; ?>" class="rounded d-block w-100" alt="Banner">

                                        </div>

                                    </div>
                                </div>
                            </div>


                            <!-- 3rd container  game info-->
                            <div class="container-fluid third-section p-0 m-0 mt-5">
                                <div class="game_info">
                                    <h1 class="text-center text-white">
                                        BASIC INFO
                                    </h1>
                                </div>
                                <div class="edit_page d-inline-block p-2" id="edit_button">
                                    <img src="https://img.icons8.com/color/48/000000/settings.png" />
                                </div>
                                <article>
                                    <div class="event_info">
                                        <div class="info1">
                                            <div class="head1">Game Name </div>
                                            <div class="body1"><?php echo strtoupper($data['game'] . ' Mobile'); ?></div>
                                        </div>

                                        <div class="info2">
                                            <div class="head1">Tournament Name </div>
                                            <div class="body1"><?php echo strtoupper($data['tournament_name']); ?></div>
                                        </div>
                                        <div class="info3">
                                            <div class="head1">Platform </div>
                                            <div class="body1"><?php echo strtoupper($data['platform']); ?></div>
                                        </div>
                                        <div class="info4">
                                            <div class="head1">Entry Fee </div>
                                            <div class="body1"><?php echo strtoupper($data['price']); ?></div>
                                        </div>
                                        <div class="info5">
                                            <div class="head1">Game Type</div>
                                            <div class="body1"><?php echo strtoupper($data['type']); ?></div>
                                        </div>
                                        <div class="info6">
                                            <div class="head1">Date & Time</div>
                                            <div class="body1"><?php echo $data['start_date'] . ' -> ' . $data['time'].' PM'; ?><br>Check-in -> <?php echo $data['check_in'];?>PM</div>
                                        </div>
                                        <div class="info7">
                                            <div class="head1">Place and Address</div>
                                            <div class="body1"><?php echo $data['address']; ?></div>
                                        </div>
                                        <div class="info8 d-block">
                                            <div id="accordion" class="">
                                                <div class="card">
                                                    <div class="card-header">
                                                        <a href="#prize" data-toggle="collapse" class="card-link d-block">
                                                            <h3>Prize</h3>
                                                        </a>
                                                    </div>
                                                    <div id="prize" class="collapse" data-parent="#accordion">
                                                        <table class="table text-center w-75 mx-auto table-striped">
                                                            <thead>
                                                                <th>Position</th>
                                                                <th>Prize</th>
                                                            </thead>
                                                            <tbody>
                                                                <?php

                                                                $search =  array_search($event_id, array_column(json_decode($prize_file), 'event_id'));
                                                                $ss = $prize_array[$search];
                                                                $i = 1;
                                                                foreach (array_slice($ss, 1) as $key) {
                                                                ?>
                                                                    <tr>
                                                                        <td><?php echo 'Position ' . $i; ?></td>
                                                                        <td><?php echo $key; ?></td>
                                                                    </tr>
                                                                <?php
                                                                    $i++;
                                                                }
                                                                ?>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="info9 d-block">
                                            <div id="accordion" class="">
                                                <div class="card">
                                                    <div class="card-header">
                                                        <a href="#community" data-toggle="collapse" class="card-link d-block">
                                                            <h3>Schedule</h3>
                                                        </a>
                                                    </div>
                                                    <div id="community" class="collapse" data-parent="#accordion">
                                                        <table class="table text-center">
                                                            <thead>
                                                                <th>Time</th>
                                                                <th>Team Limit</th>
                                                            </thead>
                                                            <tbody>
                                                                <?php

                                                                $search =  array_search($event_id, array_column(json_decode($file), 'event_id'));
                                                                $ss = $time_array[$search];
                                                                $i = 1;
                                                                foreach (array_slice($ss, 1) as $key) {
                                                                ?>
                                                                    <tr>
                                                                        <td><?php echo 'Batch ' . $i; ?></td>
                                                                        <td><?php echo $key; ?></td>
                                                                    </tr>
                                                                <?php
                                                                    $i++;
                                                                }
                                                                ?>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </article>
                            </div>
                        </section>
                    </div>
                </main>
            </div>
        </div>
    </div>


    <!-- Javascript -->
    <script src="../JavaScript/Organizer_menu.js"></script>
    <script src="Organizer_Dashboard.js"></script>
    <script>
        // document.getElementById('edit_button').style.position = 'unset';

        // window.onscroll = function () {
        //     var scroll = window.pageYOffset;

        //     console.log(scroll);

        //     if (scroll >= 790) {
        //         document.getElementById('edit_button').style.position = 'fixed';
        //         document.getElementById('edit_button').style.right = '0';
        //     } else {
        //         document.getElementById('edit_button').style.position = 'unset';
        //     }
        // }
    </script>

</body>

</html>