<?php
session_start();
$connect = new mysqli('localhost', 'root', '', 'bloodshed');


if (isset($_REQUEST['event_id'])) {
    $event_id = $_REQUEST['event_id'];
    $fetch_tournament_info = "SELECT * FROM `tournament_status` WHERE event_id = '$event_id'";
    $fetch = $connect->query($fetch_tournament_info);
    $event_detail = mysqli_fetch_assoc($fetch);
}

$count_records = "SELECT * FROM `tournament_records` WHERE event_id = '$event_id'";
if ($count = $connect->query($count_records)) {
    $count_entry = mysqli_fetch_assoc($count);
} else {
    echo $connect->error;
}


if (isset($_POST['type'])) {
    $org = $_POST['org'];
    $event_id = $_POST['event_id'];
    $player_username = $_POST['username'];
    $position = $_POST['position'];
    $prize = $_POST['prize'];
    $game = $_POST['game'];
    $type = $_POST['type'];

    $insert = "INSERT INTO tournament_records(username,event_id,player_username,position,prize,kill_ratio,game,game_type,status) VALUES ('$org','$event_id','$player_username','$position','$prize','null','$game','$type','completed')";
    if ($connect->query($insert)) {
        echo 'submit';
        exit();
    } else {
        echo $connect->error;
        exit();
    }
}

if (isset($_POST['completed'])) {
    $event_id = $_POST['event_id'];
    $update = "UPDATE `tournament_status` SET status = 'completed' WHERE `event_id` = '$event_id'";
    if ($connect->query($update)) {
        echo 'submit';
        echo $event_id;
        exit();
    } else {
        $connect->error;
        exit();
    }
}

// check user type and session
if (isset($_SESSION['username'])) {
    $role = trim($_SESSION['role']);
    if ($role == 'organizer') {
        $username = $_SESSION['username'];
    } else {
        header('Location: ../static/index.php');
    }
} else {
    header('Location: ../static/index.php');
}
$select = "SELECT * FROM `tournament_status` WHERE `username_id`='$username'";
$result = $connect->query($select);


?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Organizer Dashboard</title>
    <link rel="stylesheet" href="../css/bootstrap.css">
    <script src="../Jquery/jquery-3.4.1.js"></script>
    <script src="../js/bootstrap.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.1/css/all.min.css">
    <script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
    <link rel="stylesheet" href="Org_navbar.css">
    <link rel="stylesheet" href="sidebar.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    <link rel="stylesheet" href="../Font/fontawesome-free-5.12.0-web/css/all.css">
    <link rel="stylesheet" href="result_submit.css">
</head>

<body>
    <div class="main_navbar">
        <nav class="navbar navbar-expand-lg navbar-mainbg">
            <a class="navbar-brand navbar-logo" href="#">Bloodshed</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <i class="fas fa-bars text-white"></i>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto">
                    <div class="hori-selector">
                        <div class="left"></div>
                        <div class="right"></div>
                    </div>
                    <li class="nav-item active">
                        <a class="nav-link" href="Organizer_Dashboard.php">
                            <i class="fas fa-tachometer-alt"></i> Dashboard
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="org_profile.php">
                            <i class="far fa-address-book"></i> My Profile
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="org_profile.php">
                            <i class="far fa-chart-bar"></i> Edit Profile
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="../logout.php">
                            <i class="far fa-copy"></i> Logout
                        </a>
                    </li>
                </ul>
                <div class="nav-item profile_navbar d-flex ml-auto" style="width: 20%;">
                    <div class="profile">
                        <img src="../images/profile.jpg" style="height: 100%; width: 100%;" alt="">
                    </div>
                    <div class="profile_info text-white pl-2">
                        <span class="username d-block"><?php echo strtoupper($username); ?></span>
                        <span class="user_status d-block position-relative">ONLINE</span>
                    </div>
                </div>
            </div>
        </nav>
    </div>
    <!-- Navbar -->
    <!-- Body Content Start -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 px-0 bg-dark">
                <header>

                    <div class="hamburger">
                        <i class="nav_icon fas fa-bars"></i>
                    </div>
                    <?php include 'sidebar.php'; ?>
                </header>
                <main>
                    <div class="zoom-content">
                        <section>
                            <div class="container-fluid p-0">
                                <div class="row p-0 m-0">
                                    <div class="col-10 mx-auto">
                                        <div class="testbox">
                                            <form action="" id="form_data" method="post">
                                                <div class="banner">
                                                    <h1>Tournament Submission</h1>
                                                </div>
                                                <p>Runner Information</p>
                                                <div class="item">
                                                    <label for="name">Organizer<span>*</span></label>
                                                    <input id="org" type="text" name="org" value="<?php echo $username; ?>" readonly />
                                                </div>
                                                <div class="item">
                                                    <label for="name">Event ID<span>*</span></label>
                                                    <input id="event_id" type="text" name="event_id" value="<?php echo $event_id; ?>" readonly />
                                                </div>
                                                <div class="item">
                                                    <label for="player_username">Player username<span>*</span></label>
                                                    <input id="player_username" type="text" name="player_username" required />
                                                </div>
                                                <div class="item">
                                                    <label for="position">Position<span>*</span></label>
                                                    <input id="position" type="text" name="position" required />
                                                </div>
                                                <div class="item">
                                                    <label for="prize">Prize<span>*</span></label>
                                                    <input id="prize" type="text" name="prize" required />
                                                </div>
                                                <div class="item">
                                                    <label for="game">Game<span>*</span></label>
                                                    <input id="game" type="text" name="game" value="<?php echo $event_detail['game']; ?>" readonly />
                                                </div>
                                                <div class="item">
                                                    <label for="type">Game Type<span>*</span></label>
                                                    <input id="type" type="text" name="type" value="<?php echo $event_detail['type']; ?>" readonly />
                                                </div>
                                                <div class="btn-block">
                                                    <small>Total Entry: <?php echo $event_detail['total_player']; ?>. You enter <?php if (empty($count_entry)) {
                                                                                                                                    echo '0';
                                                                                                                                } else {
                                                                                                                                    echo count($count_entry);
                                                                                                                                };  ?></small>
                                                    <br />
                                                    <button type="button" id="submission">Submit</button>
                                                </div>


                                                <?php if (empty($count_entry)) {
                                                } else {
                                                    if ((count($count_entry)) == $event_detail['total_player']) { ?>
                                                        <label for="" class="mt-5 text-danger">Submit your tournament and click here when you done all entry of existing tournament.</label>
                                                        <button type="button" class="float-right w-100 btn-danger" name="tournament_submit" id="tournament_completed">Tournament Completed</button>
                                                <?php
                                                    }
                                                }
                                                ?>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                </main>
            </div>
        </div>
    </div>



    <!-- Hamburger Menu Link -->
    <script src="Organizer_Dashboard.js"></script>
    <!-- upper navbar link -->
    <script src="../JavaScript/Organizer_menu.js"></script>

    <script>
        $(function() {
            $('body').find('input').attr('required', 'true');

            $('#submission').click(function() {
                let org = $('#org').val();
                let event_id = $('#event_id').val();
                let player_username = $('#player_username').val();
                let position = $('#position').val();
                let prize = $('#prize').val();
                let game = $('#game').val();
                let type = $('#type').val();
                if (type == ' ') {
                    alert('Fill All Required Field');
                } else {
                    $.ajax({
                        url: 'result_submit.php',
                        type: 'post',
                        data: {
                            org: org,
                            event_id: event_id,
                            username: player_username,
                            position: position,
                            prize: prize,
                            game: game,
                            type: type
                        },
                        success: function(data) {
                            if (data == 'submit') {
                                let event_id = $('#event_id').val('');
                                let player_username = $('#player_username').val('');
                                let position = $('#position').val('');
                                let prize = $('#prize').val('');
                                let game = $('#game').val('');
                                let type = $('#type').val('');
                            } else {
                                alert('something went wrong !');
                            }
                        }
                    });
                }

            });

            $('#tournament_completed').click(function() {
                let event_id = $('#event_id').val();

                $.ajax({
                    url: 'result_submit.php',
                    type: 'post',
                    data: {
                        completed: 'completed',
                        event_id: event_id
                    },
                    success: function(data) {
                        if (data == 'submit') {
                            alert('Tournament Submitted')
                        } else {
                            alert('something went wrong !');
                        }
                    }
                });
            });
        });
    </script>
</body>

</html>