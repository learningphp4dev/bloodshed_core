<?php 

?>

<nav class="sidebar">
    <!-- profile picture -->
    <div class="profile_picture">
        <img src="../images/profile.jpg" alt="">
    </div>

    <div class="profile_name">
        <p><?php echo strtoupper($username); ?></p>
    </div>
    <ul class="nav-list">
        <li class="nav-item">
            <a href="Organizer_Dashboard.php" class="nav-link current">
                <i title="Dashboard" class="fas fa-tachometer-alt"></i>Dashboard
            </a>
        </li>
        <li class="nav-item">
            <a href="Organizer_CreateNewEvent.php" class="nav-link">
                <i title="Create Event" class="fas fa-plus-square"></i>Create Event
            </a>
        </li>
        <li class="nav-item">
            <a href="checkevent.php" class="nav-link">
                <i title="Check Event" class="fas fa-check-double"></i>Check Event
            </a>
        </li>
        <li class="nav-item">
            <a href="result_Submit.php" class="nav-link">
                <i title="Check Event" class="fas fa-check-double"></i>Submit Result
            </a>
        </li>
        <li class="nav-item">
            <a href="checkevent.php" class="nav-link">
                <i title="Activity" class="fab fa-readme"></i>Activity
            </a>
        </li>
        <li class="nav-item">
            <a href="../logout.php" class="nav-link">
                <i title="Logout" class="fas fa-sign-out-alt"></i>Logout
            </a>
        </li>
    </ul>
    <div class="social-media">
        <a href="" class="icon-link">
            <i class="fab fa-instagram"></i>
        </a>
        <a href="" cladss="icon-link">
            <i class="fab fa-twitter-square"></i>
        </a>
        <a href="" class="icon-link">
            <i class="fab fa-linkedin"></i>
        </a>
        <a href="" class="icon-link">
            <i class="fab fa-facebook"></i>
        </a>
    </div>
</nav>