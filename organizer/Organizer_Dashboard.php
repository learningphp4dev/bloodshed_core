<?php
session_start();
$connect = new mysqli('localhost', 'root', '', 'bloodshed');

// check user type and session
if (isset($_SESSION['username'])) {
    $role = trim($_SESSION['role']);
    if ($role == 'organizer') {
        $username = $_SESSION['username'];
    } else {
        header('Location: ../static/index.php');
    }
} else {
    header('Location: ../static/index.php');
}
$select = "SELECT * FROM `tournament_status` WHERE `username_id`='$username'";
$result = $connect->query($select);
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Organizer Dashboard</title>
    <link rel="stylesheet" href="../css/bootstrap.css">
    <script src="../Jquery/jquery-3.4.1.js"></script>
    <script src="../js/bootstrap.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.1/css/all.min.css">
    <script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
    <link rel="stylesheet" href="Organizer_Dashboard1.css">
    <link rel="stylesheet" href="Org_navbar.css">
    <link rel="stylesheet" href="sidebar.css">
    <link rel="stylesheet" href="../Font/fontawesome-free-5.12.0-web/css/all.css">
</head>

<body>
    <!-- Main Navbar -->
    <!-- Navbar -->
    <div class="main_navbar">
        <nav class="navbar navbar-expand-lg navbar-mainbg">
            <a class="navbar-brand navbar-logo" href="#">Bloodshed</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <i class="fas fa-bars text-white"></i>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto">
                    <div class="hori-selector">
                        <div class="left"></div>
                        <div class="right"></div>
                    </div>
                    <li class="nav-item active">
                        <a class="nav-link" href="Organizer_Dashboard.php">
                            <i class="fas fa-tachometer-alt"></i> Dashboard
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="org_profile.php">
                            <i class="far fa-address-book"></i> My Profile
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="org_profile.php">
                            <i class="far fa-chart-bar"></i> Edit Profile
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="../logout.php">
                            <i class="far fa-copy"></i> Logout
                        </a>
                    </li>
                </ul>
                <div class="nav-item profile_navbar d-flex ml-auto" style="width: 20%;">
                    <div class="profile">
                        <img src="../images/profile.jpg" style="height: 100%; width: 100%;" alt="">
                    </div>
                    <div class="profile_info text-white pl-2">
                        <span class="username d-block"><?php echo strtoupper($username); ?></span>
                        <span class="user_status d-block position-relative">ONLINE</span>
                    </div>
                </div>
            </div>
        </nav>
    </div>
    <!-- Navbar -->
    <!-- Body Content Start -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 px-0">
                <header>
                    <div class="dashb pt-4 pl-5 text-white text-uppercase text-accent-1 text-center">
                        <h1>Dashboard</h1>
                    </div>
                    <div class="hamburger">
                        <i class="nav_icon fas fa-bars"></i>
                    </div>
                    <?php include 'sidebar.php'; ?>
                </header>
                <main>
                    <div class="zoom-content">
                        <section>
                            <!-- Loader Divide into three block -->
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="containerC rounded">
                                            <div class="container-fluid">
                                                <div class="row">
                                                    <div class="col-md-4 mx-auto">
                                                        <div class="card border border-0">
                                                            <div class="box">
                                                                <div class="percent">
                                                                    <svg>
                                                                        <circle cx="70" cy="70" r="70"></circle>
                                                                        <circle cx="70" cy="70" r="70"></circle>
                                                                    </svg>
                                                                    <div class="number">
                                                                        <h1><?php
                                                                        if(!empty(mysqli_num_rows($result))){
                                                                            echo mysqli_num_rows($result);
                                                                        } else {
                                                                            echo '0';
                                                                        }
                                                                        ?></h1>
                                                                    </div>
                                                                </div>
                                                                <h3 class="text">Total Events</h3>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4 mx-auto px-0">
                                                        <div class="card border border-0">
                                                            <div class="box">
                                                                <div class="percent">
                                                                    <svg>
                                                                        <circle cx="70" cy="70" r="70"></circle>
                                                                        <circle cx="70" cy="70" r="70"></circle>
                                                                    </svg>
                                                                    <div class="number">
                                                                        <h1>80</h1>
                                                                    </div>
                                                                </div>
                                                                <h3 class="text">Participiants</h3>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4 mx-auto px-0">
                                                        <div class="card border border-0">
                                                            <div class="box">
                                                                <div class="percent">
                                                                    <svg>
                                                                        <circle cx="70" cy="70" r="70"></circle>
                                                                        <circle cx="70" cy="70" r="70"></circle>
                                                                    </svg>
                                                                    <div class="number">
                                                                        <h1>30</h1>
                                                                    </div>
                                                                </div>
                                                                <h3 class="text">Events Held</h3>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- Status of Events -->
                            <div class="container-fluid mt-5">
                                <div class="row">
                                    <div class="col-12 recently_table">
                                        <h1 class="text-center">Recently Created Events</h1>
                                        <table class="table mt-5">
                                            <thead>
                                                <th>Event Name</th>
                                                <th>Publish Date</th>
                                                <th>Start Date</th>
                                                <th>Status</th>
                                                <th>Action</th>
                                            </thead>
                                            <tbody>
                                                <?php
                                                if (!empty(mysqli_num_rows($result))) {
                                                    while($row = mysqli_fetch_assoc($result)){
                                                        ?>
                                                        <tr>
                                                        <td><?php echo $row['tournament_name']; ?></td>
                                                        <td><?php echo $row['publish_date']; ?></td>
                                                        <td><?php echo $row['start_date']; ?></td>
                                                        <td id='blink'><?php echo $row['status']; ?> <span class='dot_ani'>...</span></td>
                                                        <td><a href="<?php echo 'Organizer_EventInfo.php?event_id='.$row['event_id']; ?>" class='btn btn-warning'>View</a></td>
                                                        </tr>
                                                <?php    }
                                                } else {
                                                ?>
                                                <div class="alert-danger danger text-center pt-2 pb-2 rounded" style="min-height: 20vh;">
                                                    <h1 class="text-dark">You Haven't Create Any Tournament Yet !</h1>
                                                    <a href="Organizer_CreateNewEvent.php" class="btn m-0 btn-dark mx-auto">Create New Tournament</a>
                                                </div>
                                                <?php
                                                }
                                                ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <!-- Chart / Graph -->
                            <!-- <div class="container-fluid">
                                <div class="row">
                                    <div class="col-md-12 mx-auto">
                                        <h1 class="p-4">Total Profit [Week Wise]</h1>
                                        <div class="TotalProfit-Chart mt-5 p-3 text-center text-white">
                                            <canvas id="myChart"></canvas>
                                        </div>
                                    </div>
                                </div>
                            </div> -->
                        </section>
                    </div>
                </main>
            </div>
        </div>
    </div>


    <!-- Hamburger Menu Link -->
    <script src="Organizer_Dashboard.js"></script>
    <!-- upper navbar link -->
    <script src="../JavaScript/Organizer_menu.js"></script>
    <!-- Graph chart link -->
    <script src="org_js/index_chart.js"></script>
</body>

</html>