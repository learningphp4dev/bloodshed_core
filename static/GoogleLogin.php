<?php
session_start();
include('GoogleConfig.php');
$connect = new mysqli('localhost', 'root', '', 'bloodshed');

if(isset($_SESSION['access_token'])){
    $google_client->setAccessToken($_SESSION['access_token']);
}

global $username;
global $email;
global $familyname;

// Index
//index.php


//This $_GET["code"] variable value received after user has login into their Google Account redirct to PHP script then this variable value has been received
if (isset($_GET["code"])) {
    //It will Attempt to exchange a code for an valid authentication token.
    $token = $google_client->fetchAccessTokenWithAuthCode($_GET["code"]);


    if (!isset($token['error'])) {
        //Set the access token used for requests
        $google_client->setAccessToken($token['access_token']);

        //Store "access_token" value in $_SESSION variable for future use.
        $_SESSION['access_token'] = $token['access_token'];

        //Create Object of Google Service OAuth 2 class
        $google_service = new Google_Service_Oauth2($google_client);

        //Get user profile data from google
        $data = $google_service->userinfo->get();

        //Below you can find Get profile data and store into $_SESSION variable
        if (!empty($data['given_name'])) {
            $_SESSION['user_first_name'] = $data['given_name'];
        }

        if (!empty($data['family_name'])) {
            $_SESSION['user_last_name'] = $data['family_name'];
        }

        if (!empty($data['email'])) {
            $_SESSION['user_email_address'] = $data['email'];
        }

        if (!empty($data['gender'])) {
            $_SESSION['user_gender'] = $data['gender'];
        }

        if (!empty($data['picture'])) {
            $_SESSION['user_image'] = $data['picture'];
        }


        $username = $_SESSION['user_first_name'];
        $email = $_SESSION['user_email_address'];
        $familyname = $_SESSION['user_last_name'];
        $gender = $_SESSION['user_gender'];
    }

    $check_user_exist = "SELECT * from `register` WHERE email = '$email'";
    $result = $connect->query($check_user_exist);
    if (mysqli_num_rows($result) > 0) {
        unset($_SESSION['access_token']);
        $google_client->revokeToken();
        session_destroy();
        $_SESSION['already_registered'] = 'exist';
        header('Location: index.php');
        // exit();
    } else {
        $_SESSION['access_token'] = $token['access_token'];
        header('Location: google_modal.php');
        exit();
    }
}

?>