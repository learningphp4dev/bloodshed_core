<!-- Footer -->
<footer>
    <!-- Icons -->
    <div class="icons text-white">
        <ul class="nav">
            <li class="nav-item">
                <a href="#" class="nav-link">
                    <i class="fab fa-instagram"></i>
                </a>
            </li>
            <li class="nav-item">
                <a href="#" class="nav-link">
                    <i class="fab fa-twitter" aria-hidden="true"></i>
                </a>
            </li>
            <li class="nav-item">
                <a href="#" class="nav-link">
                    <i class="fab fa-facebook-f" aria-hidden="true"></i>
                </a>
            </li>
        </ul>
    </div>
    <hr>

    <!-- For lg snd up screen -->
    <div class="container-fluid">
        <div class="row p-0 m-0">
            <!-- First Column -->
            <div class="col-lg-3">
                <!-- Custom Navbar -> Hide from xs to md devices -->
                <div class="custom_navbar d-none d-lg-block">
                    <h3>Bloodshed</h3>
                    <hr>
                    <div class="footer-navbar">
                        <ul>
                            <li><a href="../static/">Dashboard</a></li>
                            <li><a href="../static/leaderboard.php">Leaderboards</a></li>
                        </ul>
                    </div>
                </div>

                <!-- Accordions -> Hide on large or wider screen -->
                <div id="accordion" class="d-lg-none">
                    <div class="card">
                        <div class="card-header">
                            <a href="#OG" data-toggle="collapse" class="card-link d-block">Bloodshed</a>
                        </div>
                        <div id="OG" class="collapse" data-parent="#accordion">
                            <ul class="navbar-nav">
                                <li class="nav-item"><a href="../static/" class="nav-link text-white pl-5">Dashboard</a></li>
                                <li class="nav-item"><a href="../static/leaderboard.php" class="nav-link pl-5 text-white">Leaderboards</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Second Column -->
            <div class="col-lg-3">
                <!-- Custom Navbar -> Hide from xs to md devices -->
                <div class="custom_navbar d-none d-lg-block">
                    <h3>COMMUNITY</h3>
                    <hr>
                    <div class="footer-navbar">
                        <ul>
                            <li><a href="../static/about_us.php">About</a></li>
                            <li><a href="../static/contactUs.php">Contact Us</a></li>
                        </ul>
                    </div>
                </div>

                <!-- Accordions -->
                <div id="accordion" class="d-lg-none">
                    <div class="card">
                        <div class="card-header">
                            <a href="#community" data-toggle="collapse" class="card-link d-block">COMMUNITY</a>
                        </div>
                        <div id="community" class="collapse" data-parent="#accordion">
                            <ul class="navbar-nav">
                                <li class="nav-item"><a href="../static/about_us.php" class="nav-link pl-5 text-white">About</a></li>
                                <li class="nav-item"><a href="../static/contactUs.php" class="nav-link pl-5 text-white">Contact Us</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Third Column -->
            <div class="col-lg-3">
                <!-- Custom Navbar -> Hide from xs to md devices -->
                <div class="custom_navbar d-none d-lg-block">
                    <h3>SUPPORT</h3>
                    <hr>
                    <div class="footer-navbar">
                        <ul>
                            <li><a href="../static/privacy_policy.php">Privacy Policy</a></li>
                            <li><a href="../static/terms.php">Terms of Service</a></li>
                            <li><a href="../static/legality.php">Legality</a></li>
                            <li><a href="../static/feedback_form.php">Feedback</a></li>
                        </ul>
                    </div>
                </div>

                <!-- Accordions -->
                <div id="accordion" class="d-lg-none">
                    <div class="card">
                        <div class="card-header">
                            <a href="#support" data-toggle="collapse" class="card-link d-block">SUPPORT</a>
                        </div>
                        <div id="support" class="collapse" data-parent="#accordion">
                            <ul class="navbar-nav">
                                <li class="nav-item"><a class="nav-link pl-5 text-white" href="../static/privacy_policy.php">Privacy Policy</a>
                                </li>
                                <li class="nav-item"><a class="nav-link pl-5 text-white" href="../static/terms.php">Terms of
                                        Service</a></li>
                                <li class="nav-item"><a class="nav-link pl-5 text-white" href="../static/legality.php">Legality</a></li>
                                <li class="nav-item"><a class="nav-link pl-5 text-white" href="../static/feedback_form.php">Legality</a></li>

                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Fourth Column -->
            <div class="col-lg-3">
                <!-- Custom Navbar -> Hide from xs to md devices -->
                <div class="custom_navbar d-none d-lg-block">
                    <h3>FOLLOW US</h3>
                    <hr>
                    <div class="footer-navbar">
                        <ul>
                            <li><a href="#!">Facebook</a></li>
                            <li><a href="#!">Twitter</a></li>
                            <li><a href="#!">YouTube</a></li>
                            <li><a href="#!">Instagram</a></li>
                        </ul>
                    </div>
                </div>

                <!-- Accordions -->
                <div id="accordion" class="d-lg-none w-100 bg-danger">
                    <div class="card w-100 bg-danger">
                        <div class="card-header">
                            <a href="#follow" data-toggle="collapse" class="card-link d-block">FOLLOW US</a>
                        </div>
                        <div id="follow" class="collapse" data-parent="#accordion">
                            <ul class="navbar-nav">
                                <li class="nav-item"><a class="nav-link pl-5 text-white" href="#">Facebook</a></li>
                                <li class="nav-item"><a class="nav-link pl-5 text-white" href="#">Twitter</a></li>
                                <li class="nav-item"><a class="nav-link pl-5 text-white" href="#">YouTube</a></li>
                                <li class="nav-item"><a class="nav-link pl-5 text-white" href="#">Instagram</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>