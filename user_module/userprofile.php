<?php
session_start();

if (isset($_SESSION['username']) == '') {
    header('Location: ../static/index.php');
}

$connect = new mysqli('localhost', 'root', '', 'bloodshed');

$username = $_SESSION['username'];

$select = "SELECT * FROM `register` WHERE `username` = '$username'";

$result = $connect->query($select);

$row = mysqli_fetch_array($result);
$user_event_list = $row['event_list'];
// converting user event list string to array
$list_of_event = explode(',', $row['event_list']);
$list_of_event = array_map('trim', $list_of_event);

$record = "SELECT * FROM `tournament_records` WHERE player_username = '$username'";

$result = $connect->query($record);

$player_event_record = array();
while ($player_record = mysqli_fetch_assoc($result)) {
    array_push($player_event_record, $player_record);
}

$total_amount = 0;
$total_winning = 0;
$event_list_of_record = array();
$position = array();
foreach ($player_event_record as $key) {
    foreach ($key as $show => $value) {
        if ($show == 'event_id') {
            array_push($event_list_of_record, $value);
        }
        if ($show == 'prize') {
            $total_amount += $value;
        }
        if ($show == 'position') {
            array_push($position, $value);
        }
        if ($show == 'position') {
            if ($value == '1st' || $value == '2nd' || $value == '3rd') {
                $total_winning++;
            }
        }
    }
}
// echo '<pre>';
// print_r($event_list_of_record);


$tournament_status_event_record = array();
foreach ($event_list_of_record as $tournament_show) {
    $tournament_status = "SELECT * FROM `tournament_status` WHERE event_id = '$tournament_show'";
    $result = $connect->query($tournament_status);
    $fetch_event_detail = mysqli_fetch_assoc($result);
    array_push($tournament_status_event_record, $fetch_event_detail);
}

// echo '<pre>';
// print_r($tournament_status_event_record);

// die();

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo $_SESSION['username']; ?></title>
    <link rel="stylesheet" href="../css/bootstrap.css">
    <script src="../Jquery/jquery-3.4.1.js"></script>
    <script src="../js/bootstrap.js"></script>
    <link rel="stylesheet" href="../Font/fontawesome-free-5.12.0-web/css/all.css">
    <link rel="stylesheet" href="userprofile_css.css">
    <link rel="stylesheet" href="../static_css/header.css">
    <link rel="stylesheet" href="../static_css/foot.css">
    <link rel="stylesheet" href="../static_css/hamburgermenu.css">

</head>

<body class="">
    <!-- Jumbotron -->
    <?php
    include 'user_header.php';
    ?>

    <!-- Navbar -->
    <div class="container-fluid position-relative">
        <div class="row">
            <div class="col-md-12 rat">
                <h1 class="text-center text-white"><?php if (!empty($row['fullname'])) {
                                                        echo $row['fullname'];
                                                    } else {
                                                        $row['username'];
                                                    } ?></h1>
            </div>

            <!-- Profile Picture -->
            <div class="col-md-12  fat">
                <div class="center button" onmouseover="button()">
                    <img alt="person image" src="../images/profile.jpg">
                </div>

            </div>
        </div>
    </div>

    <!-- User Stats -->
    <div class="container-fluid user_stats">
        <div class="row sat">

            <div class="col-12 pro_name text-center">
                <h1 style="color: honeydew; "><?php echo $row['fullname']; ?></h1>
            </div>


            <h1 class="col-12 user_stats-heading text-center ">
                Player Statics
            </h1>

            <div class="col-sm-6 col-md-3">
                <a href="#!">
                    <div class="earning">
                        <i class="fas fa-money-bill-alt earn "></i>
                        <h4 style="color: white; "><?php echo 'Rs ' . $total_amount; ?></h4>
                        <h6 style="color: white; ">Total earnings</h6>
                    </div>
                </a>
            </div>
            <div class="col-sm-6 col-md-3">
                <a href="#!">
                    <div class="game_played">
                        <i class="fas fa-user-tag " style="color: greenyellow; "></i>
                        <h4 style="color: white; "><?php echo count($tournament_status_event_record); ?></h4>
                        <h6 style="color: white; ">Total Participation</h6>
                    </div>
                </a>
            </div>
            <div class="col-sm-6 col-md-3">
                <a href="#!">
                    <div class="match_played">
                        <img src="https://gamingmonk.com/images/icons/matches-played.svg " alt=" ">
                        <h4 style="color: white; "><?php echo count($player_event_record); ?></h4>
                        <h6 style="color: white; ">Matches Played</h6>
                    </div>
                </a>
            </div>
            <div class="col-sm-6 col-md-3">
                <a href="#!">
                    <div class="won">
                        <img src="https://gamingmonk.com/images/icons/matches-won.svg " alt=" ">
                        <h4 style="color: white; "><?php echo $total_winning; ?></h4>
                        <h6 style="color: white; ">Matches Won</h6>
                    </div>
                </a>
            </div>
        </div>
    </div>

    <!-- Recently Stats -->
    <div class="container-fluid recent_play_section ">
        <div class="row ">

            <!-- Heading -->
            <h1 class="col-12 user_stats-heading text-center ">
                Recently Participate
            </h1>

            <!-- Recently Played -->
            <div class="col-11 mt-3 mx-auto ">
                <div class="recent_play bg-dark p-2 " style="min-height: 200px; ">
                    <div class="recent_play_icon mb-3 ">
                        <img src="https://cdn.gamingmonk.com/games_played.png" alt=" " style="height: 20px; ">
                    </div>
                    <div class="recent_play_games container-fluid">
                        <div class="row">
                            <?php
                            // print_r($list_of_event[0].'0 index');
                            // die();
                            if (!empty($list_of_event) && $list_of_event[0] != '') {
                                foreach (array_reverse($list_of_event) as $key => $value) {
                                    $check_player_tournament = "SELECT * FROM `tournament_status` WHERE `event_id` = '$value'";
                                    if ($result = $connect->query($check_player_tournament)) {
                                        $event_list = mysqli_fetch_assoc($result);
                            ?>
                                        <div class="col-sm-4 col-md-3 col-lg-2">
                                            <div class="col-4-inner">
                                                <img src="<?php echo '../' . $event_list['banner']; ?>" alt="">
                                                <p class="text-white text-center pt-2 mb-0"><?php echo $event_list['tournament_name']; ?></p>
                                                <p class="text-white text-center pt-2 mb-1">Start: <?php echo $event_list['start_date']; ?></p>
                                                <a href="../static/event.php?event_id=<?php echo $event_list['event_id']; ?>" class="btn btn-secondary d-block mx-auto mt-1">View Event</a>
                                            </div>
                                        </div>
                                <?php
                                    }
                                }
                            } else {
                                ?>
                                <div class="p-1 rounded bg-dark w-100 text-center">
                                    <p>Didn't Participate Yet! Look at our Tournament.</p>
                                    <a href="../static/tournament.php" class="btn btn-danger mx-auto">Tournament</a>
                                </div>
                            <?php
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>

    <!-- My Tournament -->
    <div class="container-fluid my_tournament ">
        <div class="row ">
            <!-- Heading -->
            <h1 class="col-12 user_stats-heading text-center ">
                My Tournament
            </h1>
            <!-- Tournament -->
            <div class="col-11 bg-dark mt-3 mx-auto ">
                <div class="tournament pt-4 pb-4 ">
                    <div class="tournament_icon ">
                        <img src="https://gamingmonk.com/images/icons/my-tournaments.svg " alt=" ">
                    </div>
                    <div class="container-fluid">
                        <div class="row">
                            <div class="tournament_content mt-3 pt-4 pb-4 ">

                                <?php
                                $i = 0;
                                foreach ($tournament_status_event_record as $player_done) {
                                ?>
                                    <div class="col-sm-5 col-md-3 col-lg-3">
                                        <div class="card">
                                            <img src="<?php echo '../' . $player_done['banner'] ?>" alt=" " class="card-img-top ">
                                            <hr class="bg-light ">
                                            <div class="card-title p-2 ">
                                                <h7 class="text-white ">
                                                    <?php echo  $player_done['tournament_name'] ?>
                                                </h7><br>
                                                <h7 class="text-white ">
                                                    <?php echo  'Rank:- ' . $position[$i] ?>
                                                </h7>
                                            </div>
                                        </div>
                                    </div>
                                <?php $i++;
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid game_id ">
        <div class="row ">
            <!-- Heading -->
            <h1 class="col-12 user_stats-heading text-center ">
                Game ID's
            </h1>
            <!-- Game_ID -->
            <div class="col-11 bg-dark mt-3 mx-auto ">
                <div class="game_id_logo ">
                    <img src="https://gamingmonk.com/images/icons/game-ids.svg " alt=" ">
                </div>
                <hr class="bg-light ">

                <div class="game_id_content ">
                    <div class="container ">
                        <div class="row ">
                            <table class="table-dark table ">
                                <tbody>
                                    <tr>
                                        <td>
                                        <th>PUBG-ID</th>
                                        </td>
                                        <td><?php echo $row['pubg_id']; ?></td>
                                    </tr>

                                    <tr>
                                        <td>
                                        <th>FORTNITE-ID</th>
                                        </td>
                                        <td><?php echo $row['fortnite_id']; ?></td>
                                    </tr>

                                    <tr>
                                        <td>
                                        <th>COD-ID</th>
                                        </td>
                                        <td><?php echo $row['cod_id']; ?></td>
                                    </tr>

                                    <tr>
                                        <td>
                                        <th>FREEFIRE_ID</th>
                                        </td>
                                        <td><?php echo $row['ff_id']; ?></td>
                                    </tr>
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
            </div>

            <div class="container ">
                <!-- <img src="https://gamingmonk.com/images/gamer-at-work.png " alt=" "> -->
                <div class="game_work_logo ">
                    <img src="https://gamingmonk.com/images/gamer-at-work.png " alt=" ">
                </div>
            </div>
        </div>
    </div>




    <!-- Footer -->

    <?php
    include '../static/footer.php';
    ?>

    <script src="../JavaScript/Home.js"></script>
    <script>
        function button() {
            document.getElementsByClassName("button")[0].classList.toggle("spin");
        }
    </script>
</body>

</html>