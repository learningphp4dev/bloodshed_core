<?php
session_start();
if (isset($_SESSION['username']) == '') {
    header('Location: ../static/index.php');
}
$connect = new mysqli('localhost','root','','bloodshed');


?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="../css/bootstrap.css">
    <script src="../Jquery/jquery-3.4.1.js"></script>
    <script src="../js/bootstrap.js"></script>
    <link rel="stylesheet" href="mymatches.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.1/css/all.min.css">
    <link rel="stylesheet" href="../static_css/header.css">
    <link rel="stylesheet" href="../static_css/foot.css">
    <link rel="stylesheet" href="../static_css/hamburger.css">
</head>

<body>
    <!-- Navbar -->
    <?php
    include 'user_header.php';
    ?>

    <div class="jumbotron profile_intro container-fluid px-0">
        <div class="intro">
            <h1>PUBG SQUAD ERANGEL</h1>
            <p>Date: 12-March-2020</p>
            <h5>Your Position: IInd <span class="pl-3">Won: $40</span></h5>
        </div>
    </div>
    <div class="container first-section">
        <div class="row">
            <div class="col-md-12 mt-1  mx-auto text-center">
                <div class="match-detail mt-5">
                    <div class="matches-table">
                        <table class="table table-borderless table-striped table-hover table-scroll-vertical table">
                            <thead>
                                <tr>
                                    <td>Rank</td>
                                    <td>Player Name</td>
                                    <td>Game ID</td>
                                    <td>Score</td>
                                    <td>Prize</td>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="rank"><img class="img-fluid" src="../images/1stPlace.jpg" alt=""></td>
                                    <td>Disha</td>
                                    <td>#12967</td>
                                    <td>4000</td>
                                    <td>₹ 1500</td>
                                </tr>
                                <tr>
                                    <td class="rank"><img class="img-fluid" src="../images/2ndPlace.jpg" alt=""></td>
                                    <td>Disha</td>
                                    <td>#12967</td>
                                    <td>3400</td>
                                    <td>₹ 1100</td>
                                </tr>
                                <tr>
                                    <td class="rank"><img class="img-fluid" src="../images/3rdPlace.jpg" alt=""></td>
                                    <td>Disha</td>
                                    <td>#12967</td>
                                    <td>3000</td>
                                    <td>₹ 700</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Footer -->
    <!-- Footer -->
    <footer>
        <!-- Icons -->
        <div class="icons text-white">
            <ul class="nav">
                <li class="nav-item">
                    <a href="#" class="nav-link">
                        <i class="fab fa-instagram"></i>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="#" class="nav-link">
                        <i class="fab fa-twitter" aria-hidden="true"></i>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="#" class="nav-link">
                        <i class="fab fa-facebook-f" aria-hidden="true"></i>
                    </a>
                </li>
            </ul>
        </div>
        <hr>
        <!-- For lg snd up screen -->
        <div class="container-fluid">
            <div class="row p-0 m-0">
                <!-- First Column -->
                <div class="col-lg-3">
                    <!-- Custom Navbar -> Hide from xs to md devices -->
                    <div class="custom_navbar d-none d-lg-block">
                        <h3>OG TOURNAMENT</h3>
                        <hr>
                        <div class="footer-navbar">
                            <ul>
                                <li><a href="#">Home</a></li>
                                <li><a href="#">Home</a></li>
                                <li><a href="#">Home</a></li>
                                <li><a href="#">Home</a></li>
                            </ul>
                        </div>
                    </div>

                    <!-- Accordions -> Hide on large or wider screen -->
                    <div id="accordion" class="d-lg-none">
                        <div class="card">
                            <div class="card-header">
                                <a href="#OG" data-toggle="collapse" class="card-link d-block">OG TOURNAMENT</a>
                            </div>
                            <div id="OG" class="collapse" data-parent="#accordion">
                                <ul class="navbar-nav">
                                    <li class="nav-item"><a href="#" class="nav-link text-white pl-5">Home</a></li>
                                    <li class="nav-item"><a href="#" class="nav-link pl-5 text-white">Home</a></li>
                                    <li class="nav-item"><a href="#" class="nav-link pl-5 text-white">Home</a></li>
                                    <li class="nav-item"><a href="#" class="nav-link pl-5 text-white">Home</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Second Column -->
                <div class="col-lg-3">
                    <!-- Custom Navbar -> Hide from xs to md devices -->
                    <div class="custom_navbar d-none d-lg-block">
                        <h3>COMMUNITY</h3>
                        <hr>
                        <div class="footer-navbar">
                            <ul>
                                <li><a href="#">About</a></li>
                                <li><a href="#">Contact Us</a></li>
                            </ul>
                        </div>
                    </div>

                    <!-- Accordions -->
                    <div id="accordion" class="d-lg-none">
                        <div class="card">
                            <div class="card-header">
                                <a href="#community" data-toggle="collapse" class="card-link d-block">COMMUNITY</a>
                            </div>
                            <div id="community" class="collapse" data-parent="#accordion">
                                <ul class="navbar-nav">
                                    <li class="nav-item"><a href="#" class="nav-link pl-5 text-white">About</a></li>
                                    <li class="nav-item"><a href="#" class="nav-link pl-5 text-white">Contact Us</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Third Column -->
                <div class="col-lg-3">
                    <!-- Custom Navbar -> Hide from xs to md devices -->
                    <div class="custom_navbar d-none d-lg-block">
                        <h3>SUPPORT</h3>
                        <hr>
                        <div class="footer-navbar">
                            <ul>
                                <li><a href="#">Privacy Policy</a></li>
                                <li><a href="#">Terms of Service</a></li>
                                <li><a href="#">Legality</a></li>
                                <li><a href="#">Refund & Cancellation Policy</a></li>
                            </ul>
                        </div>
                    </div>

                    <!-- Accordions -->
                    <div id="accordion" class="d-lg-none">
                        <div class="card">
                            <div class="card-header">
                                <a href="#support" data-toggle="collapse" class="card-link d-block">SUPPORT</a>
                            </div>
                            <div id="support" class="collapse" data-parent="#accordion">
                                <ul class="navbar-nav">
                                    <li class="nav-item"><a class="nav-link pl-5 text-white" href="#">Privacy Policy</a>
                                    </li>
                                    <li class="nav-item"><a class="nav-link pl-5 text-white" href="#">Terms of
                                            Service</a></li>
                                    <li class="nav-item"><a class="nav-link pl-5 text-white" href="#">Legality</a></li>
                                    <li class="nav-item"><a class="nav-link pl-5 text-white" href="#">Refund &
                                            Cancellation Policy</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Fourth Column -->
                <div class="col-lg-3">
                    <!-- Custom Navbar -> Hide from xs to md devices -->
                    <div class="custom_navbar d-none d-lg-block">
                        <h3>FOLLOW US</h3>
                        <hr>
                        <div class="footer-navbar">
                            <ul>
                                <li><a href="#">Facebook</a></li>
                                <li><a href="#">Twitter</a></li>
                                <li><a href="#">YouTube</a></li>
                                <li><a href="#">Instagram</a></li>
                            </ul>
                        </div>
                    </div>

                    <!-- Accordions -->
                    <div id="accordion" class="d-lg-none w-100 bg-danger">
                        <div class="card w-100 bg-danger">
                            <div class="card-header">
                                <a href="#follow" data-toggle="collapse" class="card-link d-block">FOLLOW US</a>
                            </div>
                            <div id="follow" class="collapse" data-parent="#accordion">
                                <ul class="navbar-nav">
                                    <li class="nav-item"><a class="nav-link pl-5 text-white" href="#">Facebook</a></li>
                                    <li class="nav-item"><a class="nav-link pl-5 text-white" href="#">Twitter</a></li>
                                    <li class="nav-item"><a class="nav-link pl-5 text-white" href="#">YouTube</a></li>
                                    <li class="nav-item"><a class="nav-link pl-5 text-white" href="#">Instagram</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>

    <script src="../JavaScript/Home.js"></script>
</body>

</html>