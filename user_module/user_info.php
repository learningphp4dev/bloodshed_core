<?php
session_start();
if (isset($_SESSION['username']) == '') {
    header('Location: ../static/index.php');
}
$session_username = $_SESSION['username'];

$connect = new mysqli('localhost', 'root', '', 'bloodshed');


// Select Query

$row;
$select = "SELECT * FROM `register` WHERE `username` = '$session_username'";

if ($connect->query($select)) {
    $result = $connect->query($select);
    $row = $result->fetch_assoc();
} else {
    echo 'error' . $connect->error;
}

if (isset($_POST['current_password'])) {
    $password = $row['password'];
    $check_current_password = md5($_POST['current_password']);
    // echo $check_current_password;

    if ($password === $check_current_password) {
        echo 'correct';
        exit();
    } else {
        echo 'not_correct';
        exit();
    }
}

$id = $row['id'];
if (isset($_POST['update_password'])) {
    $password = md5($_POST['update_password']);

    $update_password = "UPDATE `register` SET `password` = '$password' WHERE `register`.`id` = '$id'";

    if ($connect->query($update_password)) {
        echo 'update';
        exit();
    } else {
        echo 'fail';
        exit();
    }
}

// Insert Query
if (isset($_POST['submit'])) {
    // Tab1 Data
    $username = $_POST['username'];
    $fullname = $_POST['fullname'];
    $email = $_POST['email'];
    $mobile = $_POST['mobile'];
    $social_name = $_POST['social_name'];
    $dob = $_POST['dob'];
    $gender = $_POST['gender'];
    $country = $_POST['country'];
    $state = $_POST['state'];
    $city = $_POST['city'];
    $address = $city . '' . $state . '' . $country;
    $aboutme = $_POST['aboutme'];

    // Tab3 Data
    $pubg_id = $_POST['pubg_id'];
    $cod_id = $_POST['cod_id'];
    $ff_id = $_POST['ff_id'];
    $fortnite_id = $_POST['fortnite_id'];

    $insert = "UPDATE `register` SET fullname = '$fullname',mobile = '$mobile', social_game = '$social_name', gender = '$gender', address = '$address', about_me = '$aboutme', pubg_id = '$pubg_id', ff_id = '$ff_id', fortnite_id = '$fortnite_id', cod_id = '$cod_id' WHERE `username` = '$session_username'";

    if ($connect->query($insert)) {
        echo 'Success';
    } else {
        echo $connect->error;
    }
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Edit Profile</title>
    <link rel="stylesheet" href="../css/bootstrap.css">
    <script src="../Jquery/jquery-3.4.1.js"></script>
    <script src="../js/bootstrap.js"></script>
    <link rel="stylesheet" href="../Font/fontawesome-free-5.12.0-web/css/all.css">
    <link rel="stylesheet" href="Edit_User.css">
    <link rel="stylesheet" href="../static_css/header.css">
    <link rel="stylesheet" href="../static_css/foot.css">
    <link rel="stylesheet" href="../static_css/hamburgermenu.css">
</head>

<body>

    <!-- Navbar -->
    <?php
    include 'user_header.php';
    ?>
    <!-- Gap -->
    <div class="gap_top_main" style="height: 20vh;"></div>
    <!-- Tabs Container -->
    <div class="container-fluid second-section">
        <div class="row">

            <!-- Container / Page main heading -->
            <div class="col-12 text-center">
                <h1 class="page_heading">EDIT <span class="heading_2">PROFILE</span></h1>
                <p>Change your profile settings and gaming preferences</p>
            </div>

            <!-- Tabs -->
            <div class="col-12">
                <main>
                    <div id="tabs" class="">
                        <ul class="nav nav-pills">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" href="#user_profile">
                                    Profile
                                </a>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#password">
                                    Password
                                </a>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#game_id">
                                    Game ID's
                                </a>
                            </li>
                        </ul>
                    </div>

                    <div id="content">

                    </div>
                </main>
            </div>
            <form action="" method="post" class="w-100">
                <!-- Tabs Content -->
                <div class="col-11 mx-auto mt-4">
                    <div class="tab-content d-block" id="pills-tabContent">

                        <!-- Tab UserProfile -->
                        <div class="tab-pane fade show active" id="user_profile">
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="username">Username: </label>
                                        <input type="text" name="username" class="form-control" value="<?php echo $row['username']; ?>" readonly>
                                    </div>

                                    <div class="col-md-6">
                                        <label for="fullname">Fullname: </label>
                                        <input type="text" name="fullname" class="form-control" value="<?php if (!empty($row['fullname'])) {
                                                                                                            echo $row['fullname'];
                                                                                                        } ?>">
                                    </div>

                                    <div class="col-md-6">
                                        <label for="email">Email: </label>
                                        <input type="text" name="email" class="form-control" value="<?php echo $row['email']; ?>" readonly>
                                    </div>

                                    <div class="col-md-6">
                                        <label for="mobile">Mobile:</label>
                                        <input type="number" name="mobile" class="form-control" value="<?php if (!empty($row['mobile'])) {
                                                                                                            echo $row['mobile'];
                                                                                                        } ?>">
                                    </div>

                                    <div class="col-md-6">
                                        <label for="social">Social username:</label>
                                        <input type="text" name="social_name" class="form-control" value="<?php if (!empty($row['social_game'])) {
                                                                                                                echo $row['social_game'];
                                                                                                            } ?>">
                                    </div>

                                    <div class="col-md-6">
                                        <label for="dob">Date of Birth:</label>
                                        <input type="date" name="dob" class="form-control" value="<?php if (!empty($row['dob'])) {
                                                                                                        echo $row['dob'];
                                                                                                    } ?>">
                                    </div>

                                    <div class="col-md-6">
                                        <?php
                                        if (!empty($row['gender'])) {
                                            if (($row['gender']) == 'Male') { ?>
                                                <label for="gender" class="d-block">Gender:</label>
                                                <label for="gender">Male</label>
                                                <input type="radio" name="gender" value="Male" class="" checked>
                                                <label for="gender">Female</label>
                                                <input type="radio" name="gender" value="Female" class="">

                                            <?php } else { ?>
                                                <label for="gender">Male</label>
                                                <input type="radio" name="gender" value="Male" class="">
                                                <label for="gender">Female</label>
                                                <input type="radio" name="gender" value="Female" class="" checked>
                                        <?php }
                                        } ?>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-4">
                                        <label for="country">Country:</label>
                                        <select name="country" class="form-control" readonly>
                                            <option value="">India</option>
                                        </select>
                                    </div>
                                    <div class="col-md-4">
                                        <label for="state">State:</label>
                                        <select name="state" class="form-control">
                                            <option value=""></option>
                                            <option value="">India</option>
                                            <option value="">Indiana jones</option>
                                            <option value="">Demo</option>
                                        </select>
                                    </div>
                                    <div class="col-md-4">
                                        <label for="city">City:</label>
                                        <select name="city" class="form-control">
                                            <option value=""></option>
                                            <option value="">India</option>
                                            <option value="">Indiana jones</option>
                                            <option value="">Demo</option>
                                        </select>
                                    </div>

                                    <div class="col-12">
                                        <label for="about">About me:</label>
                                        <textarea name="aboutme" id="" class="form-control" value="<?php if (!empty($row['aboutme'])) {
                                                                                                        echo $row['aboutme'];
                                                                                                    } ?>"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Tab Password -->
                        <div class="tab-pane fade" id="password">
                            <div class="container">
                                <div class="row">
                                    <div class="col-12">
                                        <label for="current_pass_2">Current password: </label>
                                        <input type="text" id="current" class="form-control">
                                    </div>

                                    <div class="col-12">
                                        <label for="new_password">New password:</label>
                                        <input type="text" id="new_pass" class="form-control">
                                    </div>

                                    <div class="col-12">
                                        <label for="confirm_password">Confirm password:</label>
                                        <input type="text" id="confirm_pass" class="form-control">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Tab Game_ID -->
                        <div class="tab-pane fade" id="game_id">
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="pubg">PUBG ID: </label>
                                        <input type="text" name="pubg_id" class="form-control" value="<?php if (!empty($row['pubg_id'])) {
                                                                                                            echo $row['pubg_id'];
                                                                                                        } ?>">
                                    </div>

                                    <div class="col-md-6">
                                        <label for="cod">COD ID: </label>
                                        <input type="text" name="cod_id" class="form-control" value="<?php if (!empty($row['cod_id'])) {
                                                                                                            echo $row['cod_id'];
                                                                                                        } ?>">
                                    </div>

                                    <div class="col-md-6">
                                        <label for="freefire">Free Fire ID: </label>
                                        <input type="text" name="ff_id" class="form-control" value="<?php if (!empty($row['ff_id'])) {
                                                                                                        echo $row['ff_id'];
                                                                                                    } ?>">
                                    </div>

                                    <div class="col-md-6">
                                        <label for="fortnite">Fortnite: </label>
                                        <input type="text" name="fortnite_id" class="form-control" value="<?php if (!empty($row['fortnite_id'])) {
                                                                                                                echo $row['fortnite_id'];
                                                                                                            } ?>">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 mx-auto mt-5 text-center">
                    <button type="submit" name="submit" id="submit_pass" class="btn">Submit</button>
                    <button type="button" id="pass_sub" class="btn">Password Update</button>
                </div>
            </form>
        </div>
    </div>

    <!-- Footer -->
    <?php include '../static/footer.php'; ?>

    <script src="../JavaScript/Home.js"></script>
    <!-- Script -->
    <script>
        function success() {
            console.log('success');
        }

        function error() {
            console.log('error to submit');
        }

        $(function() {
            var current = false;
            var confirm = false;
            $('#pass_sub').hide();
            $('body').find('.form-control').attr('required', 'true');


            $('#current').keyup(function() {
                let current_pass = $('#current').val();
                $.ajax({
                    url: 'user_info.php',
                    type: 'post',
                    data: {
                        current_password: current_pass
                    },
                    success: function(data) {
                        if (data == 'correct') {
                            current = true;
                            $('#current').addClass('is-valid').removeClass('is-invalid');
                        } else {
                            current = false;
                            $('#current').addClass('is-invalid').removeClass('is-valid');
                        }
                    }
                });
            });

            $('#confirm_pass').keyup(function() {
                let new_pass = $('#new_pass').val();
                let confirm_pass = $('#confirm_pass').val();

                if (new_pass == confirm_pass) {
                    confirm = true;
                    console.log('true');
                    $('#new_pass').addClass('is-valid').removeClass('is-invalid');
                    $('#confirm_pass').addClass('is-valid').removeClass('is-invalid');
                    $('#pass_sub').show();
                    console.log('show')
                } else {
                    $('#pass_sub').hide();
                    console.log('hide');
                    confirm = false;
                    $('#new_pass').addClass('is-invalid').removeClass('is-valid');
                    $('#confirm_pass').addClass('is-invalid').removeClass('is-valid');
                }

            });

            $('#pass_sub').click(function() {
                let final_password = $('#confirm_pass').val();
                console.log(current + '' + confirm)
                if (current == true && confirm == true) {
                    $.ajax({
                        url: 'user_info.php',
                        type: 'post',
                        data: {
                            update_password: final_password
                        },
                        success: function(data) {
                            if (data == 'update') {
                                window.location.href = "user_info.php";
                            } else {
                                alert('Something Went Wrong. Please try again or later');
                            }
                        }
                    });
                } else {
                    alert('Fill the password correctly');
                }
            });
        });
    </script>
</body>

</html>