<!-- Modal For Login -->
<?php
// require_once('../static/GoogleConfig.php');
//Create a URL to obtain user authorization
// $login_button = $google_client->createAuthUrl();

?>
<div class="modal fade" id="login_modal" style="z-index: 11111" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" style="background-color: transparent !important; margin-top: -10px;">

            <div class="modal-body">
                <div class="content">
                    <ul class="nav nav-pills" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="pill" href="#login">Login</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="pill" href="#regis">Register</a>
                        </li>
                    </ul>

                    <div class="tab-content">
                        <div id="login" class="container tab-pane active">
                            <div class="error_msg alert alert-danger text-center" id="error_msg"></div>
                            <form action="" method="POST" class="mt-3">
                                <div class="div form-group">
                                    <label for="">Email address</label>
                                    <input class="form-control" type="email" name="login_email" id="login_email" placeholder="name@example.com" required>
                                    <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
                                </div>
                                <div class="div form-group position-relative">
                                    <label for="">Password</label>
                                    <input class="form-control" type="password" name="login_password" id="login_password" placeholder="Password" required>
                                    <i class="fas fa-eye-slash position-absolute" id="show_password" style="top:60%; right: 5px;"></i>
                                </div>
                                <div class="custom-control custom-checkbox mb-3">
                                    <input type="checkbox" class="custom-control-input" id="customCheck" name="example1">
                                    <label class="custom-control-label" for="customCheck">I agree to
                                        the<b> Term of User</b></label>
                                </div>
                                <a href="">Forget Password</a>
                                <div class="row">
                                    <button type="button" class="btn btn-info" id="login_button">Login</button>
                                    <button type="buttom" class="btn btn-danger" id="close" data-dismiss="modal">close</button>
                                </div>

                            </form>
                        </div>
                        <div id="regis" class="container tab-pane fade">
                            <h3 class="form__heading text-center"> Create Account</h3>
                            <h2 class="text-center">with Social Media</h2>
                            <div class="container-fluid">
                                <div class="error_msg alert alert-danger text-center" id="error_msg"></div>

                                <form action="">
                                    <div class="row">
                                        <div class="col">
                                            <a>
                                                </i><input type="button" onclick="window.location = '<?php echo $login_button ?>';" value="Log In With Google" class="btn google btn">
                                            </a>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <form action="" method="post" class="form" enctype="multipart/form-data">
                                <p class="">or use your email for registeration</p>

                                <div class="form-group">
                                    <label for="">Upload Profile</label>
                                    <input type="file" placeholder="" class="form-control" id="image" autocomplete="true">
                                </div>
                                <div class="form-group">
                                    <label for="">Username</label>
                                    <input type="text" placeholder="gagan007soul" class="form-control" id="username" autocomplete="true">
                                </div>
                                <div class="form-group">
                                    <label for="">Email address</label>
                                    <input type="email" placeholder="name@example.com" class="form-control" id="email" autocomplete="true">
                                    <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
                                </div>
                                <div class="form-group">
                                    <label for="">Password</label>
                                    <input type="password" placeholder="Password" class="form-control" id="password" title="Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" autocomplete="true">

                                </div>
                                <div class="form-group">
                                    <label for="">Which type you wanna choose.</label>
                                    <select name="" id="role" class="form-control">
                                        <option value="user">User</option>
                                        <option value="organizer">Organizer</option>
                                    </select>
                                </div>
                                <button type="submit" class="btn btn-secondary" id="registeration">Sign Up</button>
                                <button type="buttom" class="btn btn-danger" id="close" data-dismiss="modal">close</button>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<link rel="stylesheet" href="../static_css/login.css">

<script>
    var myInput = document.getElementById("password");

    myInput.onkeyup = function() {
        // Validate lowercase letters
        var lowerCaseLetters = /[a-z]/g;
        if (myInput.value.match(lowerCaseLetters)) {
            myInput.classList.remove("is-invalid");
            myInput.classList.add("is-valid");
        } else {
            myInput.classList.remove("is-valid");
            myInput.classList.add("is-invalid");
        }

        // Validate capital letters
        var upperCaseLetters = /[A-Z]/g;
        if (myInput.value.match(upperCaseLetters)) {
            myInput.classList.remove("is-invalid");
            myInput.classList.add("is-valid");
        } else {
            myInput.classList.remove("is-valid");
            myInput.classList.add("is-invalid");
        }

        // Validate numbers
        var numbers = /[0-9]/g;
        if (myInput.value.match(numbers)) {
            myInput.classList.remove("is-invalid");
            myInput.classList.add("is-valid");
        } else {
            myInput.classList.remove("is-valid");
            myInput.classList.add("is-invalid");
        }

        // Validate length
        if (myInput.value.length >= 8) {
            myInput.classList.remove("is-invalid");
            myInput.classList.add("is-valid");
        } else {
            myInput.classList.remove("is-valid");
            myInput.classList.add("is-invalid");
        }
    }
</script>